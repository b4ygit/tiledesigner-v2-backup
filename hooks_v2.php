<?php
function registerNewJs()
{
    if (is_page('tile-dashboard')) {
        wp_register_script('dashboardAddressJS', plugins_url('assets/lib/tileshop-dashboard-address.js', __FILE__), [], true, true);
        wp_enqueue_script('dashboardAddressJS');

        wp_localize_script('dashboardAddressJS', 'wpParams', [
            'ajax_url' => admin_url('admin-ajax.php'),
        ]);

        wp_register_script('dashboardAccountJS', plugins_url('assets/lib/tileshop-dashboard-account.js', __FILE__), [], true, true);
        wp_enqueue_script('dashboardAccountJS');

        wp_localize_script('dashboardAccountJS', 'wpParams', [
            'ajax_url' => admin_url('admin-ajax.php'),
        ]);
    }

    if (!is_page('tile-dashboard') && (!isset($_GET['action']) || empty($_GET['action']) || $_GET['action'] == 'type')) {
        //wp_register_script('tileshopTypeJS', plugins_url('assets/lib/tileshop-type.js', __FILE__), [], true, true);
        //wp_enqueue_script('tileshopTypeJS');
    }

    if (!is_page('tile-dashboard') && isset($_GET['action']) && $_GET['action'] == 'style-selected') {
        wp_register_script(
            'tileshopStyleSelectedJS',
            plugins_url('assets/lib/tileshop-style-selected.js', __FILE__),
            [],
            true,
            true
        );
        //wp_enqueue_script('tileshopStyleSelectedJS');
        //wp_localize_script('tileshopStyleSelectedJS', 'wpParams', [
        //    'ajax_url' => admin_url('admin-ajax.php'),
        //]);
    }

    if (!is_page('tile-dashboard') && isset($_GET['action']) && in_array($_GET['action'], ['cart', 'shipping', 'payment'])) {
        wp_register_script(
            'cartShippingPurchaseJS',
            plugins_url('assets/lib/tileshop-cart-shipping-purchase.js', __FILE__),
            [],
            true,
            true
        );
        wp_enqueue_script('cartShippingPurchaseJS');
        wp_localize_script('cartShippingPurchaseJS', 'wpParams', [
            'ajax_url' => admin_url('admin-ajax.php'),
        ]);
    }

    if (!is_page('tile-dashboard') && isset($_GET['action']) && in_array($_GET['action'], ['design', 'shareLink', 'editCart'])) {
        wp_register_script('fabricJS', plugins_url('assets/plugins/fabric/fabric-v3.6.1.min.js', __FILE__), [], false, true);
        //wp_enqueue_script('fabricJS');

        wp_register_script('html2CanvasJS', plugins_url('assets/html2canvas.min.js', __FILE__), [], false, true);
        //wp_enqueue_script('html2CanvasJS');

        wp_register_script('domToImageJS', plugins_url('assets/dom-to-image.min.js', __FILE__), [], false, true);
        //wp_enqueue_script('domToImageJS');

        wp_register_script(
            'tileShopDesignToolConstJS',
            plugins_url('assets/lib/tileshop-design-tool-const.js', __FILE__),
            [],
            true,
            true
        );
        //wp_enqueue_script('tileShopDesignToolConstJS');

        wp_register_script(
            'tileShopDesignToolFuncsJS',
            plugins_url('assets/lib/tileshop-design-tool-funcs.js', __FILE__),
            [],
            false,
            true
        );
        //wp_enqueue_script('tileShopDesignToolFuncsJS');

        wp_register_script(
            'tileShopDesignToolJS',
            plugins_url('assets/lib/tileshop-design-tool.js', __FILE__),
            [],
            true,
            true
        );
        wp_enqueue_script('tileShopDesignToolJS');
        wp_localize_script('tileShopDesignToolJS', 'wpParams', [
            'ajax_url' => admin_url('admin-ajax.php'),
        ]);

        wp_register_script('layerSync', plugins_url('assets/lib/layerSync.js', __FILE__), [], true, true);
        //wp_enqueue_script('layerSync');

        wp_register_script('customJS', plugins_url('assets/lib/customJS.js', __FILE__), [], true, true);
        //wp_enqueue_script('customJS');
    }
}

add_action('wp_enqueue_scripts', 'registerNewJs');

function wpb_custom_new_menu()
{
    register_nav_menu('tileshop-tool-menu', __('Tileshop Tool Menu'));
}
add_action('init', 'wpb_custom_new_menu');
