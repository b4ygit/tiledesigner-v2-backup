<?php

require_once 'tool_funcs.php';

function renderInitialData($productDetail)
{
    validateProductDetail($productDetail);

    $productOriginalTitle = __($productDetail['title'], 'tile-tool');
    $productFeaturedImage = $productDetail['featured_src'];
    // Manufacturer - Shape - Size Related Data
    $bpp = $ppa = $ppb = $ppp = $wpt = $wpb = 0;
    $color_unit_price = $terazo_price = $base_price = 0;
    $thickness = $template_configuration_data = $option_1 = $ruler = '';
    $image_url_of_on_top_shape = $image_url_of_on_add_more_shape = $image_url_of_on_background_shape = $image_url_of_on_small_preview_shape = '';
    $selectedManuID = $selectedShapeSlug = $selectedSizeSlug = $terazo_link = '';
    $selectedCategories = [];
    $priceObject = !empty($productDetail['priceObject']) ? $productDetail['priceObject'] : '';
    $priceObject = json_encode($priceObject);
    if (!empty($productDetail['manufacturerRelatedData'])) {
        $selectedManuID = $productDetail['manufacturerRelatedData']['manu_id'];
        $selectedShapeSlug = $productDetail['manufacturerRelatedData']['shape_slug'];
        $selectedSizeSlug = $productDetail['manufacturerRelatedData']['size_slug'];
        $color_unit_price = $productDetail['manufacturerRelatedData']['color_unit_price'];
        $terazo_price = $productDetail['manufacturerRelatedData']['terazo_price'];
        $terazo_link = $productDetail['manufacturerRelatedData']['terazo_link'];
        $base_price = $productDetail['manufacturerRelatedData']['base_price'];
        $thickness = $productDetail['manufacturerRelatedData']['thickness'];
        $bpp = $productDetail['manufacturerRelatedData']['bpp'];
        $ppa = $productDetail['manufacturerRelatedData']['ppa'];
        $ppb = $productDetail['manufacturerRelatedData']['ppb'];
        $ppp = $productDetail['manufacturerRelatedData']['ppp'];
        $wpt = $productDetail['manufacturerRelatedData']['wpt'];
        $wpb = $productDetail['manufacturerRelatedData']['wpb'];

        $image_url_of_on_top_shape = $productDetail['manufacturerRelatedData']['image_url_of_on_top_shape'] ?? '';
        $image_url_of_on_top_tile_shadow_shape = $productDetail['manufacturerRelatedData']['image_url_of_tiles_shadow_shape'] ?? '';
        $image_url_of_main_preview_shape = $productDetail['manufacturerRelatedData']['image_url_of_main_preview_shape'] ?? '';

        $imageOnTopAddMore = json_decode($productDetail['manufacturerRelatedData']['image_urls_of_add_more_shape'], true);
        $image_url_of_on_add_more_shape = $imageOnTopAddMore['main'];
        $image_url_of_add_more_hover_shape = $imageOnTopAddMore['hover'];

        $imageOnTopBackground = json_decode($productDetail['manufacturerRelatedData']['image_urls_of_background_shape'], true);
        $image_url_of_on_background_shape = $imageOnTopBackground['main'];
        $image_url_of_on_background_hover_shape = $imageOnTopBackground['hover'];

        $imageOnTopSmallPreview = json_decode($productDetail['manufacturerRelatedData']['image_urls_of_small_preview_shape'], true);
        $image_url_of_on_small_preview_shape = $imageOnTopSmallPreview['main'];
        $image_url_of_on_small_preview_hover_shape = $imageOnTopSmallPreview['hover'];

        $template_configuration_data = $productDetail['manufacturerRelatedData']['template_configuration_data'];
        $option_1 = $productDetail['manufacturerRelatedData']['option_1'];
        $ruler = $productDetail['manufacturerRelatedData']['option_2'];
    }

    $minimumOrder = $productDetail['minimumOrder'];
    $packagingInfo = $productDetail['packaging'];
    $sizeWithRatioVisualizer = json_encode($productDetail['sizeWithRatioVisualizer']);
    $selectedShapeName = $productDetail['shape']['name'];

    $selectedThickness = $productDetail['selectedThickness'];
    $productID = $productDetail['id'];
    $productDesignType = $productDetail['designType'];
    $productImages = $productLayers = buildLayersObject($productDetail['layers'], $productDetail['configData']);
    foreach ($productLayers as $layerKey => $layerData) {
        if ($layerKey == 'background-color') {
            continue;
        }

        $duplicateID = $layerData['duplicateID'];
        $id = $layerData['id'];
        switch ($duplicateID) {
            case 0:
                if (!empty($productImages) && !array_key_exists($id, $productImages)) {
                    unset($productImages[$id]);
                }
                break;
            default:
                $productImages[$id] = $layerData['imgSource'];
                break;
        }
    }
    $productImages = json_encode($productImages);
    $productLayers = json_encode($productLayers);

    $colorHover = get_option('color-hover-configuration');

    $tileSize = get_the_terms($productID, 'pa_' . $selectedShapeSlug . '-size');
    if ($tileSize instanceof WP_Error) {
        return '<h1 id="errorMessage">' . __('ERROR WHEN GETTING PRODUCT DETAIL') . '</h1>';
    }

    $sizeList = [];
    foreach ($tileSize as $sizeObj) {
        $sizeList[] = $sizeObj->slug;
    }
    $sizeList = json_encode($sizeList);

    $terazzo = pods_field('product', $productID, 'product_terazzo', true);
    $isCheckedTerazzo = !empty($terazzo) ? $terazzo : 0;

    $selectedCategories = json_encode($productDetail['categories']);

    $html = <<<HTML
    <input id="productOriginalTitle" type="hidden" value='$productOriginalTitle' />
    <input id="productFeaturedImage" type="hidden" value='$productFeaturedImage' />
    <input id="productImages" type="hidden" value='$productImages' />
    <input id="productLayers" type="hidden" value='$productLayers' />
    <input id="initialSizeList" type="hidden" value='$sizeList' />
    <input id="selectedCategories" type="hidden" value='$selectedCategories' />
    <input id="selectedManuID" type="hidden" value='$selectedManuID' />
    <input id="selectedSizeSlug" type="hidden" value='$selectedSizeSlug' />
    <input id="selectedShapeSlug" type="hidden" value='$selectedShapeSlug' />
    <input id="selectedShapeName" type="hidden" value='$selectedShapeName' />
    <input id="priceObject" type="hidden" value='$priceObject' />
    <input id="productID" type="hidden" value='$productID' />
    <input id="selectedThickness" type="hidden" value='$selectedThickness' />
    <input id="colorUnitPrice" type="hidden" value='$color_unit_price' />
    <input id="isCheckedTerazzo" type="hidden" value='$isCheckedTerazzo' />
    <input id="terazoPrice" type="hidden" value='$terazo_price' />
    <input id="terazoLink" type="hidden" value='$terazo_link' />
    <input id="terazzoUrl" type="hidden" value='$terazo_link' />
    <input id="basePrice" type="hidden" value='$base_price' />
    <input id="thickness" type="hidden" value='$thickness' />
    <input id="bpp" type="hidden" value='$bpp' />
    <input id="ppa" type="hidden" value='$ppa' />
    <input id="ppb" type="hidden" value='$ppb' />
    <input id="ppp" type="hidden" value='$ppp' />
    <input id="wpt" type="hidden" value='$wpt' />
    <input id="wpb" type="hidden" value='$wpb' />
    <input id="productDesignType" type="hidden" value='$productDesignType' />

    <input id="imageUrlOfOnTopShape" type="hidden" value='$image_url_of_on_top_shape' />
    <input id="defaultImageUrlOfOnTopShape" type="hidden" value='$image_url_of_on_top_shape' />

    <input id="imageUrlOfOnAddMoreShape" type="hidden" value='$image_url_of_on_add_more_shape' />
    <input id="defaultImageUrlOfOnAddMoreShape" type="hidden" value='$image_url_of_on_add_more_shape' />
    <input id="imageUrlOfOnAddMoreHoverShape" type="hidden" value='$image_url_of_add_more_hover_shape' />
    <input id="defaultImageUrlOfOnAddMoreHoverShape" type="hidden" value='$image_url_of_add_more_hover_shape' />

    <input id="imageUrlOfOnBackgroundShape" type="hidden" value='$image_url_of_on_background_shape' />
    <input id="defaultImageUrlOfOnBackgroundShape" type="hidden" value='$image_url_of_on_background_shape' />
    <input id="imageUrlOfOnBackgroundHoverShape" type="hidden" value='$image_url_of_on_background_hover_shape' />
    <input id="defaultImageUrlOfOnBackgroundHoverShape" type="hidden" value='$image_url_of_on_background_hover_shape' />

    <input id="imageUrlOfOnSmallPreviewShape" type="hidden" value='$image_url_of_on_small_preview_shape' />
    <input id="defaultImageUrlOfOnSmallPreviewShape" type="hidden" value='$image_url_of_on_small_preview_shape' />
    <input id="imageUrlOfOnSmallPreviewHoverShape" type="hidden" value='$image_url_of_on_small_preview_hover_shape' />
    <input id="defaultImageUrlOfOnSmallPreviewHoverShape" type="hidden" value='$image_url_of_on_small_preview_hover_shape' />

    <input id="imageUrlOfOnMainPreviewShape" type="hidden" value='$image_url_of_main_preview_shape' />
    <input id="defaultImageUrlOfOnMainPreviewShape" type="hidden" value='$image_url_of_main_preview_shape' />

    <input id="imageUrlOfOnTileShadowShape" type="hidden" value='$image_url_of_on_top_tile_shadow_shape' />
    <input id="defaultImageUrlOfOnTileShadowShape" type="hidden" value='$image_url_of_on_top_tile_shadow_shape' />
    
    <input id="templateConfigurationData" type="hidden" value='$template_configuration_data' />
    <input id="visualizerSizeRatio" type="hidden" value='$option_1' />
    <input id="rulerLink" type="hidden" value='$ruler' />
    <input id="colorHover" type="hidden" value='$colorHover' />
    <input id="priceValue" type="hidden" value='0.0'/>
    <input id="pricePerM2Default" type="hidden" value='0.0'/>
    <input id="packagingInfo" type="hidden" value='$packagingInfo' />
    <input id="minimumOrder" type="hidden" value='$minimumOrder' />
    <input id="sizeWithRatioVisualizer" type="hidden" value='$sizeWithRatioVisualizer' />
HTML;

    return $html;
}

function renderLayersData($productDetail)
{
    validateProductDetail($productDetail);

    $layers = buildLayersObject($productDetail['layers'], $productDetail['configData']);
    $html = '';
    foreach ($layers as $id => $layer) {
        if ($id == 'background-color') {
            continue;
        }
        // $html .= '<canvas id="'.$id.'"></canvas>';
        $html .= '<img src="" alt="' . $id . '" id="' . $id . '" />';
    }

    return $html;
}

function renderTerazo($productDetail)
{
    validateProductDetail($productDetail);

    $selectedTerazo = !empty($productDetail['selectedTerazo']) ? $productDetail['selectedTerazo'] : 0;
}

// function renderManufacturerName($productDetail)
// {
//     validateProductDetail($productDetail);

//     $manuObj = !empty($selectedManuID) ? get_post($selectedManuID) : '';
//     $manuName = !empty($manuObj) ? $manuObj->post_title : 'Unknown';
// }

function renderPatternList($productDetail)
{
    validateProductDetail($productDetail);

    $productLayers = buildLayersObject($productDetail['layers'], $productDetail['configData']);

    $patternBackgroundHtml = '<div class="pattern-list-after">';
    $patternBackgroundHtml .= '<div class="btn-pattern-item" id="layerBackground" onclick="onSelectPatternItem(this)">
                                    <img src="" alt="">
                                </div>';
    $patternBackgroundHtml .= '</div>';

    $patternListCounter = 1;
    unset($productLayers['background-color']);

    $patternListHtml = "";
    $patternListHtml .= '<ul class="pattern-list" id="pattern-list">';
    // $patternListHtml = '<ul class="pattern-list" id="pattern-list">';
    array_multisort(array_column($productLayers, 'order'), SORT_DESC, $productLayers);
    foreach ($productLayers as $layer) {
        $patternListHtml .= '<li class="btn-pattern-item" data-layer-id="child-canvas-' . $layer['id'] . '">';
        $patternListHtml .= "<input type='hidden' id='maxMinData_child-canvas-" . $layer['id'] . "' value='" . json_encode($layer['maxMinData']) . "' />";
        $patternListHtml .= '<a href="javascript:void(0);" onclick="onSelectPatternItem(this)"  data-layer-id="child-canvas-' . $layer['id'] . '">';
        $patternListHtml .= '<img src="" id="smallPreviewImage_child-canvas-' . $layer['id'] . '" />';
        $patternListHtml .= '</a>';
        $patternListHtml .= '<div class="icon-showmenu"></div>';
        $patternListHtml .= ''
            . '<div class="pattern-action-menu">
                    <button class="action-menu-up-down" onclick="onMoveUpPatternBtn(this)"><i class="ti-angle-up"></i></button>
                    <button class="btn-copy" onclick="onCopyPatternBtn(this)"></button>
                    <button class="btn-trash" onclick="onRemovePatternBtn(this)"></button>
                    <button class="action-menu-up-down" onclick="onMoveDownPatternBtn(this)"><i class="ti-angle-down"></i></button>
                </div>';
        $patternListHtml .= '</li>';
        $patternListCounter++;
    }
    $patternListHtml .= '</ul>';

    return $patternListHtml . $patternBackgroundHtml;
}


function renderColorSelectionList($productDetail)
{
    validateProductDetail($productDetail);

    $colorLimitOneLine = 13;

    if (empty($productDetail['manufacturerRelatedData'])) {
        return '';
    }

    $selectedManuID = $productDetail['manufacturerRelatedData']['manu_id'];
    $colorList = get_posts([
        'post_type' => 'layer_color',
        'post_status' => 'publish',
        'meta_key' => 'color_manufacturer',
        'meta_value' => $selectedManuID,
        'numberposts' => -1
    ]);

    $html = '<div class="row">';
    $count = 0;

    if (empty($colorList)) {
        return $html .= '</div>';
    }

    $total = count($colorList);
    $hover_color_default = get_option('color-hover-configuration');
    foreach ($colorList as $index => $color) {
        if ($count == $colorLimitOneLine) {
            $html .= '</div>';
            $html .= '<div class="row">';

            $count = 0;
        }

        $hover_color = pods_field('layer_color', $color->ID, 'hover_color', true);
        $main_color = pods_field('layer_color', $color->ID, 'main_color', true);
        $hover_color = !empty($hover_color) ? $hover_color : $hover_color_default;

        $html .= '<div class="col">
                    <label class="lbl-item"><input autocomplete="off" type="radio" name="color" class="d-none" data-debug-color="' . $main_color . '" />
                      <div class="color-item color-item-list" style="background-color: ' . $main_color . '" data-main-color="' . $main_color . '" data-hover-color="' . $hover_color . '" data-toggle="tooltip" data-placement="top" title="' . $color->post_title . '" data-label="' . $color->post_title . '"><i class="icon-check"></i></div>
                    </label>
                  </div>';

        $count++;
    }

    if ($count < $colorLimitOneLine) {
        for ($i = 0; $i < $colorLimitOneLine - $count; $i++) {
            $html .= '<div class="col"></div>';
        }
    }

    return $html .= '</div>';
}

function renderAllCategoryList($productDetail)
{
    validateProductDetail($productDetail);

    $lang = pll_get_post_language($productDetail['id']);

    $categories = get_terms([
        'taxonomy' => 'product_cat',
        'hide_empty' => false
    ]);

    $html_categories = '';
    foreach ($categories as $category) {
        $translatedCatID = pll_get_term($category->term_id, $lang);
        if (!empty($translatedCatID)) {
            $category = get_term_by('id', $translatedCatID, 'product_cat');
            $html_categories .= '<li style="display: inline-block; margin-left: 5px;">'
                . '<div class="checkbox">'
                . '<label>';
            $html_categories .= '<input autocomplete="off" type="checkbox" name="categories[]" data-name="' . $category->name . '" value="' . $category->term_id . '">' . __($category->name, 'tile-tool');
            $html_categories .= '</label>'
                . '</div></li>';
        }
    }

    return $html_categories;
}

function renderAllManuList($productDetail)
{
    $selected = null;
    if (!empty($productDetail['manufacturerRelatedData'])) {
        $selected = $productDetail['manufacturerRelatedData']['manu_id'];
    }

    $manus = get_posts([
        'post_type' => 'manufacturer',
        'post_status' => 'publish'
    ]);

    if (empty($manus)) {
        return '';
    }

    $html_manus = '';
    foreach ($manus as $i => $manu) {
        $checked = $selected == $manu->ID ? 'checked' : '';
        $html_manus .= '<input autocomplete="off" type="radio" name="manufacturer" ' . $checked . ' onclick="onSelectManufacturerManagerMode(this)" value="' . $manu->ID . '">';
        $html_manus .= __($manu->post_title, 'tile-tool');
        $html_manus .= '</input>';
    }

    return $html_manus;
}

function renderAllProductTypeList($productDetail)
{
    validateProductDetail($productDetail);

    $selected = $productDetail['designType'];
    $html = '';
    $productTypes = [
        'fixed', 'customized', 'color-only'
    ];
    foreach ($productTypes as $productType) {
        $checked = $selected == $productType ? 'checked' : '';
        $html .= '<input autocomplete="off" type="radio" name="product_design_type" ' . $checked . ' onclick="onSelectProductDesignTypeManagerMode(this)" value="' . $productType . '">';
        $html .= __($productType, 'tile-tool');
        $html .= '</input>';
    }

    return $html;
}

function renderPriceAreaPairs($productDetail)
{
    validateProductDetail($productDetail);

    $unitPriceLabel = __('<strong> per m<sup>2</sup> (usd) </strong>', 'tile-tool');
    $stepLabel = __('<strong> Step (m<sup>2</sup>) </strong>', 'tile-tool');
    $fixedPriceNoteLabel = __('<i>*Note: The smallest area node in m<sup>2</sup> will be set as minimum order and the price per tile corresponding to the node will be set as default price per tile</i>', 'tile-tool');
    $addMorePairLabel = __('Add one more pair', 'tile-tool');

    $pricePerTileLabel = __('<strong> price per tile (usd) </strong>', 'tile-tool');
    $equalLabel = __('equal', 'tile-tool');
    $areaLabel = __('<strong> area (m<sup>2</sup>) </strong>', 'tile-tool');
    $pairLabel = __('Pair', 'tile-tool');

    $priceObject = $productDetail['priceObject'];
    $tmpPricePerM2 = wp_get_object_terms($productDetail['id'],  'pa_price-per-m2');
    $pricePerM2 = (!is_wp_error($tmpPricePerM2)  && !empty($tmpPricePerM2)) ? $tmpPricePerM2[0]->name : '';
    $priceStep = !empty($priceObject['priceStep']) ? $priceObject['priceStep'] : 1;
    $priceAreaPairs = !empty($priceObject['priceAreaPair']) ? $priceObject['priceAreaPair'] : [];

    $priceArea = '';
    $i = 1;
    $countPriceAreaPair = count($priceAreaPairs);
    $removePair = ($countPriceAreaPair > 1) ? '<a href="javascript:void(0);" class="removePriceAreaPairBtn" onclick="onClickRemovePriceAreaPairManagerMode(this)"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>' : '';
    foreach ($priceAreaPairs as $area => $price) {
        $priceArea .= '<div class="row priceAreaPair">';
        $priceArea .= '<div class="col-sm-2 priceAreaPairTitle" style="text-align:center;"><strong class="pricePairTitle">' . $pairLabel . ' ' . $i . '</strong>' . $removePair . '</div>';
        $priceArea .= '<div class="col-sm-4">'
            . $pricePerTileLabel
            . '<input type="number" min="0" name="price" value="' . $price . '" />
                            </div>
                            <div class="col-sm-2" style="text-align:center;">' . $equalLabel . '</div>'
            . '<div class="col-sm-4">'
            . $areaLabel
            . '<input type="number" min="1" name="area" value="' . $area . '"/>
                            </div>
                        </div>';
        $i++;
    }

    $html  = '
        <div class="col-12" id="fixedPriceManagerBlock">
            <hr>
            <div id="defaultPriceBlock">
                <div class="row">
                    <div class="col-6" style="display:none;">
                        ' . $unitPriceLabel . '
                        <input type="number" min="1" class="pricePerM2" value="' . $pricePerM2 . '">
                    </div>
                    <div class="col-6">
                        ' . $stepLabel . '
                        <input type="number" min="1" class="priceStep" value="' . $priceStep . '">
                    </div>
                </div>
            </div>
            <hr>
            <p>
                <label style="color:#C72C1C;">' . $fixedPriceNoteLabel . '</label>
            </p>
            <p>
                <label>
                    <a href="javascript:void(0);" onclick="onClickAddPriceAreaPairManagerMode(this)" id="addPriceAreaPairBtn">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> ' . $addMorePairLabel . '
                    </a>
                </label>
            </p>
            <div id="priceAreaBlock">
                ' . $priceArea . '
            </div>
            <hr>
        </div>';

    return $html;
}

function renderLayerTags()
{
    $layerTags = get_terms([
        'taxonomy' => 'layer_tag',
        'hide_empty' => false,
    ]);

    $userCanEditPost = current_user_can('edit_posts');
    $eliminatedSlugs = [
        'testing'
    ];

    $html = '';
    foreach ($layerTags as $layerTag) {
        if (in_array($layerTag->slug, $eliminatedSlugs) && !$userCanEditPost) {
            continue;
        }
        $html .= '<label>';
        $html .= '<input autocomplete="off" type="checkbox" name="lm-tag" class="checkbox-pattern" value="' . $layerTag->term_id . '" onClick="onLoadMoreLayerWithTags()">';
        $html .= '<div class="checkbox-pattern-content d-flex align-items-center">';
        $html .= __($layerTag->name, 'tile-tool') . '<i class="ti-close ml-2"></i>';
        $html .= '</div>';
        $html .= '</label>';
    }

    return $html;
}

function renderSizeSelectionList($productDetail)
{
    validateProductDetail($productDetail);

    $selectedSizeSlug = $productDetail['selectedSizeSlug'];

    $html = '';
    foreach ($productDetail['sizes'] as $size) {
        $selected = $selectedSizeSlug == $size['slug'] ? 'selected' : '';
        $html .= '<option value="' . $size['slug'] . '" data-size-id="' . $size['term_id'] . '" ' . $selected . ' >' . $size['name'] . ' m</option>';
    }

    return [$html, count($productDetail['sizes'])];
}

function renderPriceNode($productDetail)
{
    validateProductDetail($productDetail);

    $priceObj = $productDetail['priceObject'];

    $html = '';
    foreach ($priceObj['priceAreaPair'] as $node => $price) {
        $html .= '<div class="tooltip" id="tooltip-' . $node . '">$' . $node . '/m<sup>2</sup></div>';
    }

    return $html;
}

function renderFragmentHtml($productDetail)
{
    validateProductDetail($productDetail);

    $html = '';
    if (empty($productDetail['manufacturerRelatedData'])) {
        return $html;
    }

    if (empty($template_configuration_data = $productDetail['manufacturerRelatedData']['template_configuration_data'])) {
        return $html;
    }

    $baseStyle = [
        'background-position: center;',
        'background-size: contain;',
        'background-repeat: no-repeat;',
    ];

    $thumbnails = pods_field('gallery', $template_configuration_data, 'upload_html_thumbnail', false);
    foreach ($thumbnails as $thumbnail) {
        $style = implode(' ', $baseStyle);
        $style .= 'background-image: url(' . $thumbnail['guid'] . ');';

        $html .= '<option value="' . $thumbnail['post_title'] . '" data-custom-style="' . $style . '" data-img-url="' . $thumbnail['guid'] . '">';
        $html .= '</option>';
    }

    return [$html, count($thumbnails)];
}

function renderVisualizerBgLayer($productDetail)
{
    validateProductDetail($productDetail);

    $html = '';

    $visualizerBgLayer = get_option('customized-visualizer-bg-layer-object');
    if (empty($visualizerBgLayer)) {
        return $html;
    }

    foreach ($visualizerBgLayer as $title => $imageLink) {
        $html .= '<option value="' . $imageLink . '">' . $title . '</option>';
    }

    return $html;
}
