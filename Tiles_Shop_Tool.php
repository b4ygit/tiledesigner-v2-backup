<?php
header("Access-Control-Allow-Origin: *");
/*
   Plugin Name: The Tiles.shop Tool
   Plugin URI: http://tiles.shop
   Description: a plugin to create a tool for user to build its own cenment tile patterns
   Version: 2.0
   Author: Hiep Le (Peter) - Phong (Wayne)
   Author URI: http://greentech-vn.com
   License: GRT LIC
   */
// for security, this line ensure no direct access to this plugin.php file is allowed
defined('ABSPATH') or die('No script kiddies please!');
define('PRODUCT_DEFAULT_ID', 13);
define('ACTION_DEFAULT', 'type');
define('TYPO3_HOST', 'fliesen.io');

require __DIR__ . '/vendor/autoload.php';
if (!class_exists('WP_Http')) {
    include_once ABSPATH . WPINC . '/class-http.php';
}

// esc_url(home_url('/', 'https'));
// $home_url = esc_url(home_url('/', 'https'));
$home_url = esc_url(home_url('/'));

//const API_URL = $home_url;    //'https://tileshop.local'; // Your store URL

//wayne
const API_KEY = 'ck_d4ccd4eccff1788788926fe4ff8f5113ad22f0e9'; // Your consumer key: la cai key generate lai tu user trong wordpress
const API_SEC = 'cs_04c597ac8a0de010908a9cf65a4509f056daede3'; // Your consumer secret: generate lai.

//peter
// const API_KEY = 'ck_4b1446665d7e4a8b32eaf7d9f538595fea17b575'; // Your consumer key: la cai key generate lai tu user trong wordpress
// const API_SEC = 'cs_f937088631d58b72d5be807e7f4cc7e07ad00c34'; // Your consumer secret: generate lai.

//$woocommerce = new \Automattic\WooCommerce\Client(
//    API_URL, API_KEY, API_SEC,
//    [
//      'verify_ssl' => false
//    ]
//);
// DEFAULT CONFIG PARAM FOR TILE TOOL

// GET DATA FROM GET FOR TILE TOOL

// Example: http://tileshop.local/tile-tool/?slug=hexagon&sizeId=18&catId=2&pId=13

function dd($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    die;
}

function d($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}

require_once 'common_funcs.php';
require_once 'product_funcs.php';
require_once 'backend_funcs.php';
require_once 'backend_ajax_funcs.php';
require_once 'ajax_funcs.php';

require_once 'hooks.php';
require_once 'hooks_v2.php';
require_once 'backend_hooks.php';

//
function tileToolActivated()
{
    global $jal_db_version;
    $jal_db_version = '1.0';
    global $wpdb;

    $queries = [];

    $table_name = $wpdb->prefix . 'tool_share_links';
    $queries[] = "CREATE TABLE IF NOT EXISTS $table_name (
            id integer(9) NOT NULL AUTO_INCREMENT,
            customizedData text NOT NULL,
            productID bigint(20) NOT NULL,
            shapeSlug varchar(20) NOT NULL,
            sizeID bigint(20) NOT NULL,
            catID bigint(20) NOT NULL,
            shortLink text DEFAULT '' NOT NULL,
            thickness decimal(10,2) DEFAULT 0 NOT NULL,
            terazo bool DEFAULT 0 NOT NULL,
            image_data longblob NULL,
            user_id bigint(20) NULL,
            created datetime,
            updated timestamp ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
        ) DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;";

    $wpdb->query("ALTER TABLE $table_name MODIFY catID bigint(20);");
    $wpdb->query("ALTER TABLE $table_name ADD uid bigint(20) NULL AFTER user_id;");
    $wpdb->query("CREATE INDEX tool_share_links_product_id_idx ON $table_name(productID);");
    $wpdb->query("CREATE INDEX tool_share_links_shape_slug_idx ON $table_name(shapeSlug);");
    $wpdb->query("CREATE INDEX tool_share_links_size_id_idx ON $table_name(sizeID);");

    $table_name = $wpdb->prefix . 'tool_manufacturer_data';
    $queries[] = "CREATE TABLE IF NOT EXISTS $table_name (
            manu_id integer(9) NOT NULL,
            shape_slug varchar(255) NOT NULL,
            size_slug varchar(255) NOT NULL,
            color_unit_price decimal(15,2) DEFAULT '0.00' NOT NULL,
            terazo_price decimal(15,2) DEFAULT '0.00' NOT NULL,
            terazo_link text NOT NULL,
            base_price decimal(15,2) DEFAULT '0.00' NOT NULL,
            thickness varchar(255) DEFAULT '[]' NOT NULL,
            bpp decimal(9,2) DEFAULT '0.00' NOT NULL,
            ppa decimal(9,2) DEFAULT '0.00' NOT NULL,
            ppb decimal(9,2) DEFAULT '0.00' NOT NULL,
            ppp decimal(9,2) DEFAULT '0.00' NOT NULL,
            wpt decimal(9,2) DEFAULT '0.00' NOT NULL,
            wpb decimal(9,2) DEFAULT '0.00' NOT NULL,
            image_url_of_on_top_shape text DEFAULT '' NOT NULL,
            template_configuration_data text DEFAULT '' NOT NULL,
            option_1 text NOT NULL,
            option_2 text NOT NULL,
            PRIMARY KEY (manu_id, shape_slug, size_slug)
        ) DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;";

    $wpdb->query("ALTER TABLE $table_name ADD COLUMN image_url_of_tiles_shadow_shape text NOT NULL AFTER wpb;");
    $wpdb->query("ALTER TABLE $table_name ADD COLUMN image_urls_of_small_preview_shape text NOT NULL AFTER image_url_of_on_top_shape;");
    $wpdb->query("ALTER TABLE $table_name ADD COLUMN image_urls_of_add_more_shape text NOT NULL AFTER image_urls_of_small_preview_shape;");
    $wpdb->query("ALTER TABLE $table_name ADD COLUMN image_urls_of_background_shape text NOT NULL AFTER image_urls_of_add_more_shape;");
    $wpdb->query("ALTER TABLE $table_name ADD COLUMN image_url_of_main_preview_shape text NOT NULL AFTER image_urls_of_background_shape;");

    $wpdb->query("CREATE INDEX tool_manufacturer_data_manu_id_idx ON $table_name(manu_id);");
    $wpdb->query("CREATE INDEX tool_manufacturer_data_shape_slug_idx ON $table_name(shape_slug);");
    $wpdb->query("CREATE INDEX tool_manufacturer_data_size_id_idx ON $table_name(size_slug);");

    $table_name = $wpdb->prefix . 'tool_user_addresses';
    $queries[] = "CREATE TABLE IF NOT EXISTS $table_name (
            id integer(9) NOT NULL AUTO_INCREMENT,
            first_name varchar(60) NOT NULL,
            last_name varchar(60) NOT NULL,
            name varchar(255) NOT NULL,
            phone varchar(15) NOT NULL,
            email varchar(255) NOT NULL,
            company varchar(320) NOT NULL,
            country varchar(255) NOT NULL,
            zone integer(9) NOT NULL,
            zip varchar(10) DEFAULT '',
            city varchar(255) DEFAULT '',
            type integer(1) DEFAULT 1 COMMENT 'Address Type: 1 - Shipping | 2 - Billing',
            user_id bigint(20) NULL,
            created_at datetime,
            updated_at timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (id),
            UNIQUE (name, company, type)
        ) DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    $result = [];
    foreach ($queries as $query) {
        $result[] = dbDelta($query);
    }

    $wpdb->query("CREATE INDEX tool_user_addresses_user_id_idx ON $table_name(user_id);");
    $wpdb->query("CREATE INDEX tool_user_addresses_country_idx ON $table_name(country);");
    $wpdb->query("CREATE INDEX tool_user_addresses_zone_idx ON $table_name(zone);");

    add_option('jal_db_version', $jal_db_version);

    $page = get_page_by_title('Tile Tool');
    if (!$page) {
        wp_insert_post([
            'post_title' => 'Tile Tool',
            'post_content' => '[tileShopTool]',
            'post_author' => get_current_user_id(),
            'post_status' => 'publish',
            'post_type' => 'page'
        ]);
    }
}

function frontPageRoute($action)
{
    require_once 'constants.php';
    require_once 'view_header.php';

    switch ($action) {
        case 'editCart':
        case 'shareLink':
        case 'design':
            if ($action == 'design') {
                checkRequiredParametersOnRoute(['product_id', 'shape']);
            } else {
                checkRequiredParametersOnRoute(['id']);
            }
            require_once 'view_design_tool.php';
            break;

        case 'style-selected':
            checkRequiredParametersOnRoute(['shape']);
            require_once 'view_style_selected.php';
            break;

        case 'manufacturerEdit':
            if (is_user_logged_in() && current_user_can('administrator')) {
                registerThemeStyleCss();
                require_once 'view_manufacturer.php';
                echo viewManufacturer();
            } else {
                return404();
            }
            break;

        case 'type':
        default:
            require_once 'view_type.php';
            break;
    }
}

//
function tileShopTool()
{
    do_action('myStartSession');
    frontPageRoute($_GET['action']);
}

add_shortcode('tileShopTool', 'tileShopTool');

register_activation_hook(__FILE__, 'tileToolActivated');
