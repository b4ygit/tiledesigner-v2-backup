<?php
const DEFAULT_HOVER_COLOR = '#BDBDBD';

function viewColorHoverConfiguration() {
    $colorHoverConfiguration = get_option('color-hover-configuration');
    $colorHoverConfiguration = !empty($colorHoverConfiguration) ? $colorHoverConfiguration : DEFAULT_HOVER_COLOR;

    $colorHoverTitleLabel = __('TilesShop Color Hover Configuration', 'tile-tool');
    $colorHoverNoteLabel = __('Please choose another color if you want to update it.', 'tile-tool');
    $submitBtnLabel = __('Submit', 'tile-tool');

$html = <<<HTML
<div class="wrap">
    <p><strong>$colorHoverTitleLabel</strong></p>
    <p style="color:red;">*$colorHoverNoteLabel</p>
    <p><input type="color" value="$colorHoverConfiguration" id="colorHoverConfigInput"></p>
    <p><button type="button" id="colorHoverConfigBtn">$submitBtnLabel</button></p>
</div>
HTML;
    return $html;
}
?>