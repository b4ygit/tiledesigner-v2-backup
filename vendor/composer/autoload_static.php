<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3b950318f38a13d314cbcd2e5c38eee8
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'Automattic\\WooCommerce\\' => 23,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Automattic\\WooCommerce\\' => 
        array (
            0 => __DIR__ . '/..' . '/automattic/woocommerce/src/WooCommerce',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit3b950318f38a13d314cbcd2e5c38eee8::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit3b950318f38a13d314cbcd2e5c38eee8::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
