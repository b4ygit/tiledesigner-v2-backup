Vue.component('countdown', {
  template: `
    <div class="timer-block float-right">
        <div class="timer-item">
            <p class="number">{{ hours | two_digits }}</p>
            <p class="text">Hours</p>
        </div>
        <div class="timer-item">
            <p class="number">{{ minutes | two_digits }}</p>
            <p class="text">Mins</p>
        </div>
        <div class="timer-item">
            <p class="number">{{ seconds | two_digits }}</p>
            <p class="text">Secs</p>
        </div>
    </div>
  `,
  mounted() {
    window.setInterval(() => {
        this.now = Math.trunc((new Date()).getTime() / 1000);
    },1000);
  },
  props: {
    date: {
      type: String
    }
  },
  data() {
    return {
      now: Math.trunc((new Date()).getTime() / 1000)
    }
  },
  computed: {
    dateInMilliseconds() {
      return Math.trunc(Date.parse(this.date) / 1000)
    },
    seconds() {
      return (this.dateInMilliseconds - this.now) % 60;
    },
    minutes() {
      return Math.trunc((this.dateInMilliseconds - this.now) / 60) % 60;
    },
    hours() {
      return Math.trunc((this.dateInMilliseconds - this.now) / 60 / 60) % 24;
    },
    days() {
      return Math.trunc((this.dateInMilliseconds - this.now) / 60 / 60 / 24);
    }
  }
});

Vue.filter('two_digits', (value) => {
  if (value < 0) {
    return '00';
  }
  if (value.toString().length <= 1) {
    return `0${value}`;
  }
  return value;
});