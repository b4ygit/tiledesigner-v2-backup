/** 
 * Social Share Urls Configuration
 */
jQuery('body').on('click', '#socialShareUrlsConfigBtn', function() {
    var link = adminConfigJsParams.ajax_url + '?action=upsertSocialShareUrlsConfiguration';
    jQuery.ajax({
        type: 'POST',
        url: link,
        data: {
            fbShareUrl : jQuery('#fbShareUrl').val(),
            instaShareUrl : jQuery('#instaShareUrl').val(),
            pintsShareUrl : jQuery('#pintsShareUrl').val()
        }
    }).done(function (resp) {
        resp = JSON.parse(resp);
        alert(resp.message);
        return;
    });
});
/**
 * Logo Configuration
 */
jQuery('body').on('click', '#logoConfigBtn', function() {
    var logoConfigValue = jQuery('#logoConfigValue').val();
    if(logoConfigValue.trim() == '') {
        alert('Please input valid logo image url.');
        return;
    }
    var link = adminConfigJsParams.ajax_url + '?action=upsertLogoConfiguration';
    jQuery.ajax({
        type: 'POST',
        url: link,
        data: {logoUrl : logoConfigValue}
    }).done(function (resp) {
        resp = JSON.parse(resp);
        alert(resp.message);
        return;
    });
});

/**
 * Blank Tile Configuration
 */
jQuery('body').on('click', '#blankTileConfigBtn', function() {
    var blankTileConfigValue = jQuery('#blankTileConfigValue').val();
    if(blankTileConfigValue.trim() == '') {
        alert('Please input valid product id.');
        return;
    }
    var link = adminConfigJsParams.ajax_url + '?action=upsertBlankTileConfiguration';
    jQuery.ajax({
        type: 'POST',
        url: link,
        data: {productID : blankTileConfigValue}
    }).done(function (resp) {
        resp = JSON.parse(resp);
        alert(resp.message);
        return;
    });
});

/**
 * Color Hover Configuration
 */
jQuery('body').on('click', '#colorHoverConfigBtn', function() {
    var link = adminConfigJsParams.ajax_url + '?action=upsertColorHoverConfiguration';
    jQuery.ajax({
        type: 'POST',
        url: link,
        data: {colorHover : jQuery('#colorHoverConfigInput').val()}
    }).done(function (resp) {
        resp = JSON.parse(resp);
        alert(resp.message);
        return;
    });
});

/**
 * Customized Product Price Configuration
 */
jQuery('#addVisualizerBgLayerPairBtn').click(function () {
    var count = jQuery('#visualizerBgLayerPair tr.pricePair').length;
    if(count === 4) {
        alert('You reach the limit of nodes (8 nodes).');
        return;
    }

    var removeBtn = '<a href="javascript:void(0);" class="removeVisualizerBgLayerPairBtn">remove pair</a>';
    jQuery('#visualizerBgLayerPair .pricePair .priceAreaPairRemoveBtn').each(function() {
        if(!jQuery(this).has('a.removeVisualizerBgLayerPairBtn').length) {
            jQuery(this).append(removeBtn);
        }
    });

    var html = '<tr class="pricePair">';
    html += '<td class="priceAreaPairTitle">Layer Name ' + (++count) + '</td>';
    html += '<td><input type="text" name="bg-layer-name"  /></td>';
    html += '<td>~ Image Link </td>';
    html += '<td><input type="text" name="bg-layer-image-link" /></td>';
    html += '<td class="priceAreaPairRemoveBtn"><a href="javascript:void(0);" class="removeVisualizerBgLayerPairBtn">remove pair</a></td>';
    html += '</tr>';

    jQuery('#visualizerBgLayerPair').append(html);
});

jQuery('body').on('click', '.removeVisualizerBgLayerPairBtn', function () {
    jQuery(this).closest('.pricePair').remove();

    var count = jQuery('#visualizerBgLayerPair tr.pricePair').length;
    if(count === 1) {
        jQuery('#visualizerBgLayerPair tr.visualizerBgLayer').find('.removeVisualizerBgLayerPairBtn').remove();
    }

    jQuery('#visualizerBgLayerPair tr.pricePair .priceAreaPairTitle').each(function (index) {
        jQuery(this).html('Layer Name ' + (++index) + ' ');
    });
});

jQuery('body').on('click', '#visualizerBackgroundLayerSubmitBtn', function() {
    var visualizerBgLayerObj = {};

    var isFulFillVisualizerPair = 1;
    jQuery('#visualizerBgLayerPair tr.pricePair').each(function (index, value) {
        var name = jQuery(value).find('input[name=bg-layer-name]').val();
        var link = jQuery(value).find('input[name=bg-layer-image-link]').val();

        if(name === '' || link === '') {
            isFulFillVisualizerPair *= 0;
            return;
        }

        visualizerBgLayerObj[name]= link;
    });

    if(isFulFillVisualizerPair == 0) {
        alert('Please fulfill valid pair of name and image link.');
        return;
    }

    var link = adminConfigJsParams.ajax_url + '?action=upsertCustomizedVisualizerBgLayerObject';
    jQuery.ajax({
        url: link,
        type: "POST",
        data: {visualizerBgLayerObj: visualizerBgLayerObj}
    }).done(function (resp) {
        resp = JSON.parse(resp);
        if(!resp.success) {
            alert(resp.message);
            return;
        }
        alert(resp.message);
        location.reload();
    });
});

jQuery('#addPriceAreaPairBtn').click(function () {
    var count = jQuery('#priceAreaBlock tr.pricePair').length;
    if(count === 8) {
        alert('You reach the limit of nodes (8 nodes).');
        return;
    }

    var removeBtn = '<a href="javascript:void(0);" class="removePriceAreaPairBtn">remove pair</a>';
    jQuery('#priceAreaBlock .pricePair .priceAreaPairRemoveBtn').each(function() {
        if(!jQuery(this).has('a.removePriceAreaPairBtn').length) {
            jQuery(this).append(removeBtn);
        }
    });

    var html = '<tr class="pricePair">';
    html += '<td class="priceAreaPairTitle">Pair ' + (++count) + '</td>';
    html += '<td><input type="number" min="0" name="percent" /> (%)</td>';
    html += '<td>equal</td>';
    html += '<td><input type="number" min="0" name="area" /> (m<sup>2</sup>)</td>';
    html += '<td class="priceAreaPairRemoveBtn"><a href="javascript:void(0);" class="removePriceAreaPairBtn">remove pair</a></td>';
    html += '</tr>';

    jQuery('#priceAreaBlock').append(html);
});

jQuery('body').on('click', '.removePriceAreaPairBtn', function () {
    jQuery(this).closest('.pricePair').remove();

    var count = jQuery('#priceAreaBlock tr.pricePair').length;
    if(count === 1) {
        jQuery('#priceAreaBlock tr.pricePair').find('.removePriceAreaPairBtn').remove();
    }

    jQuery('#priceAreaBlock tr.pricePair .priceAreaPairTitle').each(function (index) {
        jQuery(this).html('Pair ' + (++index) + ' ');
    });
});

jQuery('body').on('click', '#customizedPriceSubmitBtn', function() {
    var priceObject = {};

    // Validate priceStep
    var priceStep = jQuery('.priceStep').val();
    if(priceStep == undefined || priceStep === '' || parseFloat(priceStep) < 0) {
        alert("Please enter valid price step (equal or greater than 1)");
        return;
    }
    priceObject['priceStep'] = priceStep;

    priceObject['priceAreaPair'] = {};
    var isFulFillPriceAreaPair = 1;
    jQuery('#priceAreaBlock tr.pricePair').each(function (index, value) {
        var percent = jQuery(value).find('input[name=percent]').val();
        var area = jQuery(value).find('input[name=area]').val();

        if(percent === '' || area === '' || parseFloat(area) < 0 || parseFloat(percent) < 0) {
            isFulFillPriceAreaPair *= 0;
            return;
        }

        priceObject['priceAreaPair'][area]= percent;
    });

    if(isFulFillPriceAreaPair == 0) {
        alert('Please fulfill valid pair of percent and area. (Percent or area must be equal or greater than 0)');
        return;
    }

    var link = adminConfigJsParams.ajax_url + '?action=upsertCustomizedPriceObject';
    jQuery.ajax({
        url: link,
        type: "POST",
        data: {priceObject: priceObject}
    }).done(function (resp) {
        resp = JSON.parse(resp);
        if(!resp.success) {
            alert(resp.message);
            return;
        }
        alert(resp.message);
    });
});