jQuery(document).ready(function ($) {
  if (isMobileBrowser()) {
    $("div.tiles-block").addClass("nohover");
    $(".tiles-block div.img-shape").addClass("nohover");
    $("body").off("mouseenter mouseleave", ".tiles-block");
    $("body").off(
      "mouseenter mouseleave",
      ".shape-list .row .col-auto .choose-shape"
    );
  }

  var choose_shape_val;
  $('input[name="choose-shape"]').change(function (e) {
    // console.log(e.target.value)
    // get value of this shape
    choose_shape_val = e.target.value;

    if (choose_shape_val) {
      $("#btn-next-step").removeClass("disabled");
      $(".tiles-block").removeClass("show-first-time");
    } else {
      $("#btn-next-step").addClass("disabled");
    }
  });
  choose_shape_val = $('input[name="choose-shape"]:checked').val();

  if (choose_shape_val) {
    $(".tiles-block").removeClass("show-first-time");
  }

  $("#btn-next-step").click(function () {
    if (!choose_shape_val) {
      alert("Please choose shape.");
      return;
    }

    window.location.href =
      window.location.origin +
      window.location.pathname +
      "?action=style-selected&shape=" +
      choose_shape_val;
  });
});
