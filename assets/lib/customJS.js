var testInterval = setInterval(function () {
  if (!$("#overlay").length) {
    var query = window.location.search;
    var urlParam = new URLSearchParams(query);
    var startVisualizer = urlParam.get("startVisualizer");
    if (startVisualizer == 1) {
      console.log(startVisualizer);
      $("#toolVisualizarModalCenter").modal("show");
      clearInterval(testInterval);
    }
  }
}, 100);
