var layerCanvas = {};
var resetCanvas = {};
var originalCanvas = {};
var recentColorList = [];
var layerId = "";
var hoverLayerId = "";

var order_count = 0;
var tmpSize = 400;
var screenRatio = 1;
var colorArray = [];
var colorTotalPrice = 0;
var nodeList = [];
var nodeListTxt = [];
var tmpHoverColor = "";
var currentSelectedColor = "";
var m_shapeSize = {};

var isHover = false;
var isReload = false;

var defaultColor = "#636363";
var defaultHoverColor = "#4F94F5";

var hiddenCanvasID = "hidden-canvas";
var hiddenCanvas = new fabric.Canvas(hiddenCanvasID);
hiddenCanvas.enableRetinaScaling = true;
hiddenCanvas.imageSmoothingEnabled = true;
hiddenCanvas.svgViewportTransformation = true;
$("#" + hiddenCanvasID).hide();
$("#" + hiddenCanvasID)
  .parents(".canvas-container")
  .hide();

const FRAGMENT_IMAGE_SIZE = 100;
const MOVE_NODE_LIST = [1, 5, 20, 50];
const MOVE_STEP = 1;
const ROTATION_NODE_LIST = [1, 5, 15, 45];
const ROTATION_STEP = 1;
const ROTATION_MIN = 0;
const ROTATION_MAX = 360;
const SCALE_NODE_LIST = [1, 5, 10, 25, 50];
const SCALE_STEP = 0.01;
const SKEW_NODE_LIST = [1, 5, 10, 25, 50];
const SKEW_STEP = 0.01;
const RECENT_COLOR_MAXIMUM = 13;
const TYPO3_PRODUCT_TYPE = 3;
const TYPO3_TYPE = "Tx_Tileshop_Tile";
const TYPO3_INTERNAL_URL = "t3://page?uid=20";
const TYPO3_URL = "https://api.tiles.design/rest/tile";
const IsMobileBrowser = isMobileBrowser();
const CROP_PIXEL = 1;
const CHANGE_DESIGN_CAPTURE_COLOR_RED = 221;
const CHANGE_DESIGN_CAPTURE_COLOR_GREEN = 220;
const CHANGE_DESIGN_CAPTURE_COLOR_BLUE = 214;
const CHANGE_DESIGN_CAPTURE_COLOR_OPACITY = 255;
