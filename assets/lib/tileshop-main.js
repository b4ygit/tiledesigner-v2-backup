function startLoading() {
  // add the overlay with loading image to the page
  // var over =
  //   '<div id="overlay">' +
  //   '<img id="loading" src="' +
  //   $("#loadingImg").val() +
  //   '">' +
  //   "</div>";
  var over =
    '<div id="overlay"><div id="loading" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>';
  $(over).appendTo("body");
  $("body").css("overflow", "hidden");
}

function stopLoading() {
  $("#overlay").remove();
  $("body").css("overflow", "");
}

function checkIsIos() {
  return (
    (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) ||
    (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform))
  );
}

function checkIsSafariBrowser() {
  var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
  // var isSafari = window.safari !== undefined;
  // var isChromeOnIOS = !!navigator.userAgent.match('CriOS');
  // var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

  return isSafari;
}

function isMobileBrowser() {
  if (
    /Mobi|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    ) ||
    !!navigator.userAgent.match("CriOS")
  ) {
    return true;
  }

  return false;
}

function checkIsImageExist(imgSrc) {
  return new Promise(function (resolve, reject) {
    $.ajax({
      async: true,
      type: "HEAD",
      url: imgSrc,
      complete: function (xhr, textStatus) {
        resolve(xhr.status != 404);
      },
    });
  });
}

$("body").on("click touchend", ".change-language", function () {
  var langCode = $(this).data("lang-code");
  var currentWindowOrigin = window.location.origin;
  var currentWindowHref = window.location.href;
  var currentWindowSearch = window.location.search;
  currentWindowHref = currentWindowHref.replace(currentWindowSearch, "");
  window.location.href = currentWindowHref.replace(
    currentWindowOrigin,
    currentWindowOrigin + "/" + langCode
  );
});

$("body").on("hidden.bs.modal", "#loginModalCenter", function () {
  $(this).find('input[name="username"]').val("");
  $(this).find('input[name="password"]').val("");
  $(this).find(".info-error").html("").removeClass("show").addClass("hide");
});

$("body").on("submit", "#loginForm", function (e) {
  startLoading();
  e.preventDefault();

  var username = $(this).find('input[name="username"]').val();
  username = username.trim();

  var password = $(this).find('input[name="password"]').val();
  password = password.trim();

  if (username == "" || password == "") {
    $(this).find(".info-error").removeClass("hide").addClass("show");
    stopLoading();
    return false;
  }

  var security = $(this).find('input[name="security"]').val();

  $.ajax({
    url: wpParams.ajax_url + "?action=customAjaxLogin",
    type: "POST",
    data: {
      username: username,
      password: password,
      security: security,
    },
  }).done(function (resp) {
    resp = JSON.parse(resp);
    if (!resp.success) {
      console.log("Login error: " + resp.message);
      $("#loginForm").find(".info-error").html(resp.message);
      $("#loginForm").find(".info-error").removeClass("hide").addClass("show");
      stopLoading();
      return false;
    }

    location.reload();
  });
});

$("body").on("hidden.bs.modal", "#signupModalCenter", function () {
  $(this).find('input[name="email"]').val("");
  $(this).find('input[name="username"]').val("");
  $(this).find('input[name="password"]').val("");
  $(this).find('input[name="repassword"]').val("");
  $(this).find(".info-error").html("").removeClass("show").addClass("hide");
});

$("body").on("submit", "#signupForm", function (e) {
  startLoading();
  e.preventDefault();

  var email = $(this).find('input[name="email"]').val();
  email = email.trim();

  var username = $(this).find('input[name="username"]').val();
  username = username.trim();

  var password = $(this).find('input[name="password"]').val();
  password = password.trim();

  var repassword = $(this).find('input[name="repassword"]').val();
  repassword = repassword.trim();

  if (email == "" || username == "" || password == "" || repassword == "") {
    $(this).find(".info-error").removeClass("hide").addClass("show");
    stopLoading();
    return false;
  }

  if (password !== repassword) {
    $(this).find(".info-error").removeClass("hide").addClass("show");
    stopLoading();
    return false;
  }

  var security = $(this).find('input[name="signonsecurity"]').val();

  $.ajax({
    url: wpParams.ajax_url + "?action=customAjaxSignup",
    type: "POST",
    data: {
      email: email,
      username: username,
      password: password,
      repassword: repassword,
      security: security,
    },
  }).done(function (resp) {
    resp = JSON.parse(resp);
    if (!resp.success) {
      console.log("Signup error: " + resp.message);
      $("#signupForm").find(".info-error").html(resp.message);
      $("#signupForm").find(".info-error").removeClass("hide").addClass("show");
      stopLoading();
      return false;
    }

    location.reload();
  });
});

var queryString = window.location.search;
var urlParams = new URLSearchParams(queryString);
var showHeader = urlParams.get("showHeader");

if ($("#is-typo3-referrer").val() == 1 || showHeader == 0) {
  $("#header, #backBtnDesignTool, #sectionSubBtnHeader").addClass("hide");
  /*

} else {
  $("#typo3FixCssStyleSelected").css("margin-top", "70px");

  $("#headerTypeTool").css("margin-top", "55px");

   */
  $("#toolResponsive").css("margin-top", "-17px");
  $("#tool-section-align").css("margin-top", "20px");

}
