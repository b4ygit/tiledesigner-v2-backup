<?php
function viewCustomizedPrice() {
    $customizedPriceObject= get_option('customized-price-object');

    $priceStep = $priceAreaPairs = '';
    if(!empty($customizedPriceObject)) {
        $priceStep = $customizedPriceObject['priceStep'];
        $priceAreaPairs = $customizedPriceObject['priceAreaPair'];
    }

    $headerLabel = __('TilesShop Customized Price Option', 'tile-tool');
    $noteLabel = __('<i>*Note: The smallest area node in m<sup>2</sup> will be set as minimum order and the percent discount per tile corresponding to the node will be set as default price per tile</i>', 'tile-tool');
    $addMoreLabel = __('Add one more pair', 'tile-tool');
    $pairLabel = __('Pair ', 'tile-tool');
    $equalLabel = __('equal ', 'tile-tool');
    $submitBtnLabel = __('Submit', 'tile-tool');


$html = <<<HTML
<div class="wrap">
    <p>
        <strong>$headerLabel</strong>
    </p>
    <p>
        <strong> Step (m<sup>2</sup>) </strong>
        <input type="number" min="0" class="priceStep" value="$priceStep">
    </p>
    <hr />
    <p>
        <label style="color:#C72C1C;">$noteLabel</label>
    </p>
    <p>
        <label>
            <a href="javascript:void(0);" id="addPriceAreaPairBtn">$addMoreLabel</a>
        </label>
    </p>
    <table class="table" id="priceAreaBlock" style="font-weight: bold;">
HTML;
    if(!empty($customizedPriceObject)) :
        $count = 1;
        $removePair = (count($priceAreaPairs) > 1) ? '<a href="javascript:void(0);" class="removePriceAreaPairBtn">remove pair</a>' : '';
        foreach($priceAreaPairs as $area => $percent) {
            $html .= '<tr class="pricePair">';
            $html .= '<td class="priceAreaPairTitle">' . $pairLabel . ' ' . $count . '</td>';
            $html .= '<td><input type="number" min="0" name="percent" value="' . $percent . '" /> (%)</td>';
            $html .= '<td>' . $equalLabel . '</td>';
            $html .= '<td><input type="number" min="0" name="area" value="' . $area . '"/> (m<sup>2</sup>)</td>';
            $html .= '<td class="priceAreaPairRemoveBtn">' . $removePair . '</td>';
            $html .= '</tr>';
            $count++;
        }
    else :
$html .= <<<HTML
        <tr class="pricePair">
            <td class="priceAreaPairTitle">$pairLabel 1</td>
            <td><input type="number" min="0" name="percent" /> (%)</td>
            <td>$equalLabel</td>
            <td><input type="number" min="0" name="area" /> (m<sup>2</sup>)</td>
            <td class="priceAreaPairRemoveBtn"></td>
        </tr>
HTML;
endif;
$html .= <<<HTML
    </table>
    <p><button type="button" id="customizedPriceSubmitBtn">$submitBtnLabel</button></p>
</div>
HTML;
    return $html;
}
?>