<?php

require_once 'view_style_selected_funcs.php';
require_once 'tool_funcs.php';

$_shape = $_GET['shape'];

$initialProducts = renderProduct($_shape);
$initialCategories = renderCategories();

$shapeObj = get_term_by('slug', $_shape, 'pa_shape');
$shapeLabel = strtoupper($shapeObj->name);

$blankTileUrl .= '&shape=' . $_shape;

$header = renderHeader('style');
$menu = renderCollapseMenu();
$loginModalHtml = renderLoginModal();
$signUpModalHtml = renderSignupModal();

$html = <<<HTML
    <input type="hidden" autocomplete="off" name="selectedShape" value="$_shape" />
    <input type="hidden" autocomplete="off" name="selectedCategory" value="" />
  <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

  <!-- Add your site or application content here -->
  <div id="root">
    
    $header
  
    <!-- $menu -->

    <main id="main">
      <div class="style-sticker">
        <!-- gallery -->
       <section class="header-block position-relative reset-max-width">
        <div class="container pt-3">
          <div class="title-box">
            <h3 class="tool-title">$shapeLabel DESIGNS</h3>
            <p class="paragraph">Select a design to customize it or start with a <a  class="blue" href="$blankTileUrl">Blank tile</a></p>
          </div>
          </div>
        </section>
      
      </div>
      <section id="typo3FixCssStyleSelected">
        <div class="container py-1" id="style-selected-contaienr">
          <div class="row justify-content-center">
            $initialProducts
        </div>
      </section>
    </main>
<footer class="footer-design">
            <div class="footer-design-wrap border-top"">
             <p class="text-welcome m-0">SELECT A DESIGN OR START FROM SCRATCH</p>
              <div class="container-fluid p-0">
                <div class="row" style="float: right;">
             
                  <div class="col-12 col-md-auto f-block-height" style="padding: 0 !important;">
                    <a href="$blankTileUrl" class="link grey-style-next large btn-next-step w-100" >
                    
                    USE A BLANK TILE</a>
                  </div>
                  <div class="col-12 col-md-auto f-block-height" style="padding-left: 0 !important;">
                    <a href="javascript:void(0);"  id="style-selected-next-btn" class="link disabled blue-style large btn-next-step w-100">
                    <img alt="" src="https://app.tiles.design/media/uploads/assets/customize.png" class="mr-2">
                    CUSTOMIZE</a>
                  </div>
                </div>
              </div>
            </div>
          </footer>
   
  </div>

HTML;

echo $html;
