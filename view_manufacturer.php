<?php
function viewManufacturer()
{
    $manuID = !empty($_GET['manuID']) ? $_GET['manuID'] : '';
    if(empty($manuID) || empty(get_post($manuID))) return404();

    global $wpdb;
    $manuData = $wpdb->get_results( $wpdb->prepare(
        "SELECT * FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d",
        $manuID
    ));

    if(empty($manuData)) $manuData = [];
    $manuTitle = get_post($manuID)->post_title;

    $rowData = [];
    $manuShapes = pods_field ( 'manufacturer', $manuID, 'shape', false );
    foreach($manuShapes as $i => $manuShape) {
        $manuShapeSizes = pods_field ( 'manufacturer', $manuID, 'supported_' . $manuShape['slug'] . '_sizes', false );
        if(!empty($manuShapeSizes)) {
            $rowData[$manuShape['slug']] = [];
            foreach($manuShapeSizes as $k => $manuShapeSize) {
                $rowData[$manuShape['slug']][$manuShapeSize['slug']] = [];
                $manuShapeSizeData = [
                    'color_unit_price' => '',
                    'terazo_price' => '',
                    'base_price' => '',
                    'thickness' => '[]',
                    'bpp' => '',
                    'ppa' => '',
                    'ppb' => '',
                    'ppp' => '',
                    'wpt' => '',
                    'wpb' => '',
                    'image_url_of_main_preview_shape' => '',
                    'image_url_of_tiles_shadow_shape' => '',
                    'image_url_of_on_top_shape' => '',
                    'image_urls_of_add_more_shape' => '',
                    'image_urls_of_small_preview_shape' => '',
                    'image_urls_of_background_shape' => '',
                    'template_configuration_data' => '',
                    'option_1' => '',
                    'option_2' => ''
                ];
                $rowData[$manuShape['slug']][$manuShapeSize['slug']] = (object) $manuShapeSizeData;
            }
        }
    }

    if(!empty($manuData)) {
        foreach($manuData as $i => $manuRow) {
            if(isset($rowData[$manuRow->shape_slug][$manuRow->size_slug])) {
                $rowData[$manuRow->shape_slug][$manuRow->size_slug] = $manuRow;
            }
        }
    }

    $galleries = get_posts([
        'post_type' => 'gallery',
        'post_status' => 'publish',
        'numberposts'       => -1,
    ]);

    $selectGalleryList = [];
    foreach($galleries as $i => $gallery) {
        $selectGalleryList[$gallery->ID] = $gallery->post_title;
    }

    $html = '';

    $colorUnitPriceLabel = __('Color Unit Price:', 'tile-tool');
    $terazoPriceLabel = __('Terazo Price:', 'tile-tool');
    $terazoLinkLabel = __('Terazo Link:', 'tile-tool');
    $basePriceLabel = __('Base Price:', 'tile-tool');
    $thicknessLabel = __('Thickness:', 'tile-tool');
    $boxesPerPalletLabel = __('Boxes per pallet:', 'tile-tool');
    $piecesPerAreaLabel = __('Pieces per area:', 'tile-tool');
    $piecesPerBoxLabel = __('Pieces per box:', 'tile-tool');
    $piecesPerPalletLabel = __('Pieces per pallet:', 'tile-tool');
    $weightPerTileLabel = __('Weight per tile:', 'tile-tool');
    $weightPerBoxLabel = __('Weight per box:', 'tile-tool');
    $imageUrlOnTopTileShadowLabel = __('Tile Shadow:', 'tile-tool');
    $imageUrlOnTopTileMainPreviewLabel = __('Image url of on top main preview shape:', 'tile-tool');
    $imageUrlOnTopLabel = __('Image url of on top shape:', 'tile-tool');
    $imageUrlOnTopAddMoreLabel = __('Image url of on add more shape:', 'tile-tool');
    $imageUrlOnTopAddMoreHoverLabel = __('Image url of on add more hover shape:', 'tile-tool');
    $imageUrlOnTopBackgroundLabel = __('Image url of on background shape:', 'tile-tool');
    $imageUrlOnTopBackgroundHoverLabel = __('Image url of on background hover shape:', 'tile-tool');
    $imageUrlOnTopSmallPreviewLabel = __('Image url of on small preview shape:', 'tile-tool');
    $imageUrlOnTopSmallPreviewHoverLabel = __('Image url of on small preview hover shape:', 'tile-tool');
    $templateConfigLabel = __('Template configuration data:', 'tile-tool');
    $option1Label = __('Visualizer Size Ratio:', 'tile-tool');
    $option2Label = __('Grid layer:', 'tile-tool');
    $updateBtnlabel = __('Update', 'tile-tool');

    $html .= <<<HTML
<main>
    <div class="container-fluid">
        <h1>$manuTitle</h1>
        <input type="hidden" id="manuID" value="$manuID">
HTML;
foreach($rowData as $shape => $shapeData) :
    $html .= '<div class="row dataRow">';
    $html .= '<input type="hidden" class="shapeSlug" value="' . $shape .'">';
    $html .= '<div class="col-sm-12">';
    $html .= '<p><h2>' . strtoupper($shape) . '</h2></p>';
    foreach($shapeData as $size => $sizeData) :
        $imageOnTopShape = $sizeData->image_url_of_on_top_shape ?? '';
        $imageOnTopTileShadowShape = $sizeData->image_url_of_tiles_shadow_shape ?? '';
        $imageOnTopMainPreview = $sizeData->image_url_of_main_preview_shape ?? '';

        $imageOnTopAddMore = json_decode($sizeData->image_urls_of_add_more_shape, true);
        $imageOnTopAddMoreShape = $imageOnTopAddMore['main'];
        $imageOnTopAddMoreHoverShape = $imageOnTopAddMore['hover'];

        $imageOnTopBackground = json_decode($sizeData->image_urls_of_background_shape, true);
        $imageOnTopBackgroundShape = $imageOnTopBackground['main'];
        $imageOnTopBackgroundHoverShape = $imageOnTopBackground['hover'];

        $imageOnTopSmallPreview = json_decode($sizeData->image_urls_of_small_preview_shape, true);
        $imageOnTopSmallPreviewShape = $imageOnTopSmallPreview['main'];
        $imageOnTopSmallPreviewHoverShape = $imageOnTopSmallPreview['hover'];

        $html .= '<div class="accordion-block">';
        $html .= '<input type="hidden" class="sizeSlug" value="' . $size .'">';
        $html .= '<button style="text-align:center;" type="button" data-toggle="collapse" data-target="#' . $shape . '_' . $size . '"><h3>' . $size . '</h3></button>';
        $html .= '<div class="description collapse row" id="' . $shape . '_' . $size . '" style="color:#16171a;">';
        $html .= '<div class="col-sm-4">';
        $html .= '<p><strong>'. $colorUnitPriceLabel . '</strong><input class="w-100" required type="number" min="0" step="0.01" name="color_unit_price" value="' . $sizeData->color_unit_price .'" /></p>';
        $html .= '<p><strong>'. $terazoPriceLabel . '</strong><input class="w-100" required type="number" min="0" step="0.01" name="terazo_price" value="' . $sizeData->terazo_price .'" /></p>';
        $html .= '<p><strong>'. $terazoLinkLabel . '</strong><input class="w-100" required type="text" name="terazo_link" value="' . (isset($sizeData->terazo_link) ? $sizeData->terazo_link : '' ) .'" /></p>';
        $html .= '<p><strong>' . $basePriceLabel . '</strong><input class="w-100" required type="number" min="0" step="0.01" name="base_price" value="' . $sizeData->base_price .'" /></p>';
        $html .= '<p><strong>' . $thicknessLabel . '</strong><input class="w-100" type="text" name="thickness" value="' . implode(',',json_decode($sizeData->thickness, true)) .'" /></p>';
        $html .= '</div>';
        $html .= '<div class="col-sm-4">';
        $html .= '<p><strong>' . $boxesPerPalletLabel . '</strong><input class="w-100" required type="number" min="0" name="bpp" value="' . $sizeData->bpp .'" /></p>';
        $html .= '<p><strong>' . $piecesPerAreaLabel . '</strong><input class="w-100" required type="number" min="0" step="0.01" name="ppa" value="' . $sizeData->ppa .'" /></p>';
        $html .= '<p><strong>' . $piecesPerBoxLabel . '</strong><input class="w-100" required type="number" min="0" step="0.01" name="ppb" value="' . $sizeData->ppb .'" /></p>';
        $html .= '<p><strong>' . $piecesPerPalletLabel . '</strong><input class="w-100" required type="number" min="0" step="0.01" name="ppp" value="' . $sizeData->ppp .'" /></p>';
        $html .= '<p><strong>' . $weightPerTileLabel . '</strong><input class="w-100" required type="number" min="0" step="0.01" name="wpt" value="' . $sizeData->wpt .'" /></p>';
        $html .= '</div>';
        $html .= '<div class="col-sm-4">';
        $html .= '<p><strong>' . $weightPerBoxLabel . '</strong><input class="w-100" required type="number" min="0" step="0.01" name="wpb" value="' . $sizeData->wpb .'" /></p>';
        $html .= '<p><strong>' . $imageUrlOnTopTileShadowLabel . '</strong><input class="w-100" required type="text" name="image_url_of_on_top_tile_shadow_shape" value="' . $imageOnTopTileShadowShape .'" /></p>';
        $html .= '<p><strong>' . $imageUrlOnTopLabel . '</strong><input class="w-100" required type="text" name="image_url_of_on_top_shape" value="' . $imageOnTopShape .'" /></p>';
        $html .= '<p><strong>' . $imageUrlOnTopAddMoreLabel . '</strong><input class="w-100" required type="text" name="image_url_of_on_add_more_shape" value="' . $imageOnTopAddMoreShape .'" /></p>';
        $html .= '<p><strong>' . $imageUrlOnTopAddMoreHoverLabel . '</strong><input class="w-100" required type="text" name="image_url_of_on_add_more_hover_shape" value="' . $imageOnTopAddMoreHoverShape .'" /></p>';
        $html .= '<p><strong>' . $imageUrlOnTopBackgroundLabel . '</strong><input class="w-100" required type="text" name="image_url_of_on_background_shape" value="' . $imageOnTopBackgroundShape .'" /></p>';
        $html .= '<p><strong>' . $imageUrlOnTopBackgroundHoverLabel . '</strong><input class="w-100" required type="text" name="image_url_of_on_background_hover_shape" value="' . $imageOnTopBackgroundHoverShape .'" /></p>';
        $html .= '<p><strong>' . $imageUrlOnTopSmallPreviewLabel . '</strong><input class="w-100" required type="text" name="image_url_of_on_small_preview_shape" value="' . $imageOnTopSmallPreviewShape .'" /></p>';
        $html .= '<p><strong>' . $imageUrlOnTopSmallPreviewHoverLabel . '</strong><input class="w-100" required type="text" name="image_url_of_on_small_preview_hover_shape" value="' . $imageOnTopSmallPreviewHoverShape .'" /></p>';
        $html .= '<p><strong>' . $imageUrlOnTopTileMainPreviewLabel . '</strong><input class="w-100" required type="text" name="image_url_of_main_preview_shape" value="' . $imageOnTopMainPreview .'" /></p>';
        $html .= '<p><strong>' . $templateConfigLabel . '</strong>';
        $html .= '<select name="template_configuration_data" class="form-control" style="height: 45px;padding: 6px 12px;">';
        foreach ($selectGalleryList as $v => $t) {
            $html .= '<option value="' . $v . '" ' . (($v == $sizeData->template_configuration_data) ? 'selected' : '') . ' >';
            $html .= $t;
            $html .= '</option>';
        }
        $html .= '';
        $html .= '</select>';
        $html .= '</p>';
        $html .= '<p><strong>' . $option1Label . '</strong><input class="w-100" type="number" name="option_1" value="' . ($sizeData->option_1 ? $sizeData->option_1 : 1)  .'" /></p>';
        $html .= '<p><strong>' . $option2Label . '</strong><input class="w-100" type="text" name="option_2" value="' . $sizeData->option_2 .'" /></p>';
        $html .= '</div>';
        $html .= '<div class="col-sm-4 col-sm-offset-4" style="margin-top:10px;">';
        $html .= '<button class="btn btn-white updateManufacturerData" style="text-align:center;">' . $updateBtnlabel . '</button>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
    endforeach;
    $html .= '</div>';
    $html .= '</div>';
endforeach;
$html .= <<<HTML
    </div>
</main>
HTML;
    return $html;
}
?>