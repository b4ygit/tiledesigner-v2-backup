<?php
function getLayerMinMaxData()
{
    if ($_GET['action'] !== 'getLayerMinMaxData' && empty($_GET['layerId'])) {
        $return = json_encode(['success' => 0, 'message' => 'Error while getting related gallery.']);
    } else {
        $layerId = $_GET['layerId'];
        $min_limit_moving = pods_field('layer', $layerId, 'min_limit_moving', true);
        $max_limit_moving = pods_field('layer', $layerId, 'max_limit_moving', true);
        $min_limit_rotate = pods_field('layer', $layerId, 'min_limit_rotate', true);
        $max_limit_rotate = pods_field('layer', $layerId, 'max_limit_rotate', true);
        $min_limit_scaling = pods_field('layer', $layerId, 'min_limit_scaling', true);
        $max_limit_scaling = pods_field('layer', $layerId, 'max_limit_scaling', true);
        $return = json_encode(['success' => 1, 'message' => [
            'min_limit_moving' => $min_limit_moving,
            'max_limit_moving' => $max_limit_moving,
            'min_limit_rotate' => $min_limit_rotate,
            'max_limit_rotate' => $max_limit_rotate,
            'min_limit_scaling' => $min_limit_scaling,
            'max_limit_scaling' => $max_limit_scaling
        ]]);
    }

    echo $return;
    wp_die();
}
function checkPermissionIfProductDefault($productIDDefault = 13)
{
    switch ($productIDDefault) {
        case 13:
            return (!current_user_can('administrator') || !current_user_can("manage_tiles_shop")) ? false : true;
            break;
        default:
            return true;
            break;
    }
}

function return404()
{
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
}

function renderManuList()
{
    $manus = get_posts([
        'post_type' => 'manufacturer',
        'post_status' => 'publish'
    ]);
    $html_manus = '';
    if (!empty($manus)) {
        foreach ($manus as $i => $manu) {
            $html_manus .= '<option value="' . $manu->ID . '">';
            $html_manus .= __($manu->post_title, 'tile-tool');
            $html_manus .= '</option>';
        }
    }

    return $html_manus;
}

function renderCatList($lang)
{
    $categories = getCategories();
    $html_categories = '';
    foreach ($categories as $category) {
        $translatedCatID = pll_get_term($category->term_id, $lang);
        if (!empty($translatedCatID)) {
            $category = get_term_by('id', $translatedCatID, 'product_cat');
            $html_categories .= '<li style="display: inline-block; margin-left: 5px;">'
                . '<div class="checkbox">'
                . '<label>';
            $html_categories .= '<input type="checkbox" name="categories[]" data-name="' . $category->name . '" value="' . $category->term_id . '">' . __($category->name, 'tile-tool');
            $html_categories .= '</label>'
                . '</div></li>';
        }
    }

    return $html_categories;
}

function renderLayerCatList()
{
    $layerCats = get_terms([
        'taxonomy' => 'layer_cat',
        'hide_empty' => false
    ]);

    $html = '';
    foreach ($layerCats as $layerCat) {
        $html .= '<li>'
            . '<div class="checkbox">'
            . '<label>';
        $html .= '<input type="checkbox" name="modalCategories[]" value="' . $layerCat->term_id . '">' . __($layerCat->name, 'tile-tool');
        $html .= '</label>'
            . '</div></li>';
    }

    return $html;
}

function renderShapeSizeList()
{
    $shapes = get_terms([
        'taxonomy' => 'pa_shape',
        'hide_empty' => false
    ]);
    $html_shape = null;
    foreach ($shapes as $shape) {
        $html_shape .= '<li>'
            . '<div class="checkbox">'
            . '<label>'
            . '<input type="checkbox" name="shape[]" data-shape-name="' . $shape->name . '" value="' . $shape->slug . '">' . __($shape->name, 'tile-tool')
            . '</label>'
            . '</div>';
        $sizes = get_terms([
            'taxonomy' => 'pa_' . $shape->slug . '-size',
            'hide_empty' => false
        ]);
        if (!empty($sizes)) {
            $html_shape .= '<ul>';
            foreach ($sizes as $size) {
                $sizeDetail = json_decode($size->description, true);
                $ppa = !empty($sizeDetail['ppa']) ? $sizeDetail['ppa'] : '';
                $html_shape .= '<li>'
                    . '<div class="checkbox">'
                    . '<label>'
                    . '<input type="checkbox" class="sizeChkBox" data-shape-slug="' . $shape->slug . '" name="shape[' . $shape->slug . '][]" value="' . $size->slug . '">'
                    . __($size->name, 'tile-tool')
                    . (!empty($ppa) ? ' ~ ' . $ppa . ' piece(s) per m<sup>2</sup>' : '')
                    . '</label>'
                    . '</div></li>';
            }
            $html_shape .= '</ul>';
        }
        $html_shape .= '</li>';
    }

    return $html_shape;
}

function renderShapes()
{
    $shapes = get_terms([
        'taxonomy' => 'pa_shape',
        'hide_empty' => false
    ]);

    $html_shape = null;
    foreach ($shapes as $shape) {
        $html_shape .= '<option value="' . $shape->slug . '">' . __($shape->name, 'tile-tool') . '</option>';
    }

    return $html_shape;
}

function renderCategories()
{
    $categories = getCategories();
    $html_categories = '';
    foreach ($categories as $category) {
        $html_categories .= '<li data-cat-id="' . $category->term_id . '" data-cat-slug="' . $category->slug . '"><a href="#' . $category->slug . '" data-toggle="tab" class="headerCategory">'
            . __($category->name, 'tile-tool') . '</a></li>';
    }

    return $html_categories;
}

function getGalleries()
{
    if ($_GET['action'] !== 'getGalleries' && empty($_GET['templateConfigurationData'])) {
        $return = json_encode(['success' => 0, 'message' => 'Error while getting related gallery.']);
    } else {
        $g_templateConfigurationData = $_GET['templateConfigurationData'];
        $thumbnails = pods_field('gallery', $g_templateConfigurationData, 'upload_html_thumbnail', false);
        $return_thumbnails = [];
        foreach ($thumbnails as $i => $thumbnail) {
            $return_thumbnails[$thumbnail['post_title']] = $thumbnail['guid'];
        }
        $return = json_encode(['success' => 1, 'message' => $return_thumbnails]);
    }

    echo $return;
    wp_die();
}

function getHtmlFragment()
{
    if ($_POST['action'] !== 'getHtmlFragment' && (empty($_POST['imgSrc']) || empty($_POST['postTitle']) || empty($_POST['templateConfigurationData']))) {
        $return = json_encode(['success' => 0, 'message' => 'Error while getting related html fragment.']);
    } else {
        $g_postTitle = $_POST['postTitle'];
        $g_imgSrc = $_POST['imgSrc'];
        $g_templateConfigurationData = $_POST['templateConfigurationData'];
        $fragment_files = pods_field('gallery', $g_templateConfigurationData, 'upload_html_fragment_files', false);

        $message = [
            'imgSrc' => $g_imgSrc,
            'htmlFragment' => ''
        ];
        foreach ($fragment_files as $i => $fragment_file) {
            if ($fragment_file['post_title'] == $g_postTitle) {
                $message['htmlFragment'] = $fragment_file['guid'];
                break;
            }
        }
        $return = json_encode(['success' => 1, 'message' => $message]);
    }

    echo $return;
    wp_die();
}

function getProductDetail()
{
    $productDetail = null;

    $g_action = !empty($_GET['action']) ? $_GET['action'] : null;

    if (!empty($g_action)) {
        switch ($g_action) {
            case 'productDetail':
                $g_productID = !empty($_GET['productID']) ? $_GET['productID'] : null;
                $g_shapeSlug = !empty($_GET['shapeSlug']) ? $_GET['shapeSlug'] : null;
                $g_sizeID = !empty($_GET['sizeID']) ? $_GET['sizeID'] : null;
                $g_catSlug = !empty($_GET['catSlug']) ? $_GET['catSlug'] : null;
                $g_isManager = !empty($_GET['isManager']) ? $_GET['isManager'] : null;

                if ((!empty($g_productID) && !empty($g_shapeSlug)
                    && !empty($g_sizeID) && !empty($g_catSlug)) || !empty($g_isManager)) {
                    if (!checkPermissionIfProductDefault($g_productID)) {
                        return404();
                    }
                    $existedProduct = get_posts([
                        'ID' => $g_productID,
                        'post_type' => 'product',
                        'post_status' => 'publish',
                        'tax_query' => [
                            'relation' => 'AND',
                            [
                                'taxonomy'  => 'pa_' . $g_shapeSlug . '-size',
                                'field'     => 'term_id',
                                'terms'     => [$g_sizeID],
                                'include_children' => false
                            ],
                            [
                                'taxonomy'  => 'pa_shape',
                                'field'     => 'slug',
                                'terms'     => [$g_shapeSlug],
                                'include_children' => false
                            ],
                            [
                                'taxonomy'  => 'product_cat',
                                'field'     => 'slug',
                                'terms'     => [$g_catSlug],
                                'include_children' => false
                            ]
                        ]
                    ]);
                    if (!empty($g_isManager) || !empty($existedProduct)) {
                        $productDetail = _getProductDetail($g_productID);
                        if (empty($productDetail)) return [];
                        $selectedThickness = wc_get_product_terms($productDetail['id'], 'pa_thickness', [
                            'fields' => 'all'
                        ]);
                        $productDetail['selectedThickness'] = !empty($selectedThickness) ? $selectedThickness[0]->slug : '';
                        if (!$g_isManager) {
                            $tileSize = get_term_by('term_id', $g_sizeID, 'pa_' . $g_shapeSlug . '-size');
                            if ($tileSize) {
                                $_sizeSlug = $tileSize->slug;
                                $_shapeSlug = $g_shapeSlug;
                            }
                        } elseif ($productDetail['productDesignType'] === 'fixed') {
                            $selectedShape = wc_get_product_terms($productDetail['id'], 'pa_shape', [
                                'fields' => 'all'
                            ]);
                            $selectedSize = wc_get_product_terms($productDetail['id'], 'pa_' . $selectedShape[0]->slug . '-size', [
                                'fields' => 'all'
                            ]);
                            $_sizeSlug = $selectedSize[0]->slug;
                            $_shapeSlug = $selectedShape[0]->slug;
                        } else {
                            $_sizeSlug = $_shapeSlug = '';
                        }

                        global $wpdb;
                        $manuRelatedData = $wpdb->get_row($wpdb->prepare(
                            "SELECT * FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d AND shape_slug LIKE %s AND size_slug LIKE %s LIMIT 1",
                            $productDetail['productManufacturer'],
                            $_shapeSlug,
                            $_sizeSlug
                        ));

                        if (!empty($manuRelatedData)) {
                            $productDetail['manufacturerShapeSizeRelatedData'] = (array) $manuRelatedData;
                            $thicknessList = $productDetail['manufacturerShapeSizeRelatedData']['thickness'];
                            $defaultThicknessList = json_decode($thicknessList, true);
                            if (!empty($defaultThicknessList)) {
                                $productDetail['selectedThickness'] = !empty($productDetail['selectedThickness']) ? $productDetail['selectedThickness'] : $defaultThicknessList[0];
                                asort($defaultThicknessList);
                                $thicknessTxt = '[';
                                $thicknessTxt .= implode(',', $defaultThicknessList);
                                $thicknessTxt .= ']';
                                $thicknessList = $thicknessTxt;
                            }
                            $productDetail['manufacturerShapeSizeRelatedData']['thickness'] = $thicknessList;
                        }
                    }
                }
                break;
            case 'shareLink':
                $g_linkID = !empty($_GET['id']) ? $_GET['id'] : null;
                global $wpdb;
                $shareObject = $wpdb->get_row($wpdb->prepare(
                    "SELECT * FROM " . ($wpdb->prefix . 'tool_share_links') . " WHERE id=%d",
                    $g_linkID
                ));

                if (empty($shareObject->productID) || !checkPermissionIfProductDefault($shareObject->productID)) {
                    return404();
                }

                $productDetail = _getProductDetail($shareObject->productID);
                if (empty($productDetail)) return [];
                $customizedData = json_decode($shareObject->customizedData, true);
                $productDetail['layers'] = [];
                foreach ($customizedData as $canvasId => $canvasData) {
                    if ('background-color' == $canvasId) continue;
                    $existedLayer = get_post($canvasData['id']);
                    if (!empty($existedLayer) && 'publish' == $existedLayer->post_status) {
                        $productDetail['layers'][$canvasData['id']] = [
                            'src' => $canvasData['imgSource'],
                            'name' => $canvasData['name']
                        ];
                    }
                }
                $productDetail['selectedThickness'] = !empty($shareObject->thickness) ? $shareObject->thickness : '';
                $productDetail['selectedTerazo'] = !empty($shareObject->terazo) ? $shareObject->terazo : 0;

                $tileSize = get_term_by('term_id', $shareObject->sizeID, 'pa_' . $shareObject->shapeSlug . '-size');

                $manuRelatedData = [];
                if ($tileSize) {
                    global $wpdb;
                    $manuRelatedData = $wpdb->get_row($wpdb->prepare(
                        "SELECT * FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d AND shape_slug LIKE %s AND size_slug LIKE %s LIMIT 1",
                        $productDetail['productManufacturer'],
                        $shareObject->shapeSlug,
                        $tileSize->slug
                    ));
                }

                break;
            case 'editCartItem':
                $g_orderItemId = !empty($_GET['itemId']) ? $_GET['itemId'] : null;
                $g_orderId = !empty($_GET['orderId']) ? $_GET['orderId'] : null;
                $cartData = getTilesShopCheckout($g_orderId);
                if (empty($cartData) || empty($cartData[$g_orderItemId])) {
                    break;
                }

                $orderItemData = $cartData[$g_orderItemId];
                $oid_ProductDetail = $orderItemData['productDetail'];

                if (!checkPermissionIfProductDefault($oid_ProductDetail['productID'])) {
                    return404();
                }

                $productDetail = _getProductDetail($oid_ProductDetail['productID']);
                if (empty($productDetail)) return [];
                $cartItemLayerCanvas = $orderItemData['layerCanvas'];
                $productDetail['layers'] = [];
                foreach ($cartItemLayerCanvas as $canvasId => $canvasData) {
                    if ('background-color' == $canvasId) continue;
                    $existedLayer = get_post($canvasData['id']);
                    if (!empty($existedLayer) && 'publish' == $existedLayer->post_status) {
                        $productDetail['layers'][$canvasData['id']] = [
                            'src' => $canvasData['imgSource'],
                            'name' => $canvasData['name']
                        ];
                    }
                }
                $productDetail['selectedThickness'] = !empty($oid_ProductDetail['thickness']) ? $oid_ProductDetail['thickness'] : '';
                $productDetail['selectedTerazo'] = !empty($oid_ProductDetail['terazo']) ? $oid_ProductDetail['terazo'] : 0;

                $tileSize = get_term_by('term_id', $oid_ProductDetail['sizeID'], 'pa_' . $oid_ProductDetail['shapeSlug'] . '-size');

                $manuRelatedData = [];
                if ($tileSize) {
                    global $wpdb;
                    $manuRelatedData = $wpdb->get_row($wpdb->prepare(
                        "SELECT * FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d AND shape_slug LIKE %s AND size_slug LIKE %s LIMIT 1",
                        $productDetail['productManufacturer'],
                        $oid_ProductDetail['shapeSlug'],
                        $tileSize->slug
                    ));
                }

                break;
        }
        if (!empty($manuRelatedData)) {
            $productDetail['manufacturerShapeSizeRelatedData'] = (array) $manuRelatedData;
            $thicknessList = $productDetail['manufacturerShapeSizeRelatedData']['thickness'];
            $defaultThicknessList = json_decode($thicknessList, true);
            if (!empty($defaultThicknessList)) {
                $productDetail['selectedThickness'] = !empty($productDetail['selectedThickness']) ? $productDetail['selectedThickness'] : $defaultThicknessList[0];
                asort($defaultThicknessList);
                $thicknessTxt = '[';
                $thicknessTxt .= implode(',', $defaultThicknessList);
                $thicknessTxt .= ']';
                $thicknessList = $thicknessTxt;
            }
            $productDetail['manufacturerShapeSizeRelatedData']['thickness'] = $thicknessList;

            if ($productDetail['productDesignType'] == 'customized') {
                $base_price = $productDetail['manufacturerShapeSizeRelatedData']['base_price'];
                $priceAreaPair = [];
                foreach ($productDetail['priceObject']['priceAreaPair'] as $area => $price) {
                    $priceAreaPair[$area] = $base_price - ($base_price * $price / 100);
                }
                $productDetail['priceObject']['priceAreaPair'] = $priceAreaPair;
            }
        }
    }

    return $productDetail;
}

function _getManuShapeSizeRelatedData($manuID, $shapeSlug, $sizeSlug)
{
    $manuRelatedData = [
        'color_unit_price' => 0,
        'terazo_price' => 0,
        'base_price' => 0,
        'thickness' => [],
        'bpp' => 0,
        'ppa' => 0,
        'ppb' => 0,
        'ppp' => 0,
        'wpt' => 0,
        'wpb' => 0,
        'image_url_of_on_top_shape' => '',
        'template_configuration_data' => '',
        'option_1' => '',
        'option_2' => ''
    ];
    if (!empty($manuID) && !empty($shapeSlug) && !empty($sizeSlug)) {
        global $wpdb;
        $manuRelatedData = (array) $wpdb->get_row($wpdb->prepare(
            "SELECT * FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d AND shape_slug LIKE %s AND size_slug LIKE %s LIMIT 1",
            $manuID,
            $shapeSlug,
            $sizeSlug
        ));
    }

    return $manuRelatedData;
}

function getManuShapeSizeRelatedData()
{
    if (
        $_GET['action'] !== 'getManuShapeSizeRelatedData'
        && (empty($_GET['manuID']) || empty($_GET['shapeSlug']) || empty($_GET['sizeSlug']))
    ) {
        echo json_encode(['success' => 0, 'message' => 'Error while getting related data.']);
        wp_die();
    }
    $g_shapeSlug = $_GET['shapeSlug'];
    $g_manuID = $_GET['manuID'];
    $g_sizeSlug = $_GET['sizeSlug'];

    $manuRelatedData = _getManuShapeSizeRelatedData($g_manuID, $g_shapeSlug, $g_sizeSlug);

    echo json_encode(['success' => 1, 'message' => $manuRelatedData]);
    wp_die();
}

function getThicknessManagerList()
{
    if (
        $_GET['action'] !== 'getThicknessManagerList'
        && (empty($_GET['manuID']) || empty($_GET['shapeSlug']) || empty($_GET['sizeSlug']))
    ) {
        echo json_encode(['success' => 0, 'message' => __('Error while getting related data.', 'tile-tool')]);
        wp_die();
    }
    $g_shapeSlug = $_GET['shapeSlug'];
    $g_sizeSlug = $_GET['sizeSlug'];
    $g_manuID = $_GET['manuID'];

    $manuRelatedData = _getManuShapeSizeRelatedData($g_manuID, $g_shapeSlug, $g_sizeSlug);
    $thicknessList = $manuRelatedData['thickness'];
    $defaultThicknessList = json_decode($thicknessList, true);
    if (!empty($defaultThicknessList)) {
        $defaultSelected = $defaultThicknessList[0];
        asort($defaultThicknessList);
        $thicknessTxt = '[';
        $thicknessTxt .= implode(',', $defaultThicknessList);
        $thicknessTxt .= ']';
        $thicknessList = $thicknessTxt;
        echo json_encode(['success' => 1, 'message' => $thicknessList, 'defaultSelected' => $defaultSelected]);
    } else {
        echo json_encode(['success' => 0, 'message' => __('Thickness data is empty. Please check in manufacturer data.', 'tile-tool')]);
    }

    wp_die();
}

function getShapeSizeByManufacturer()
{
    if ($_GET['action'] !== 'getShapeSizeByManufacturer' || empty($_GET['manuID'])) {
        echo json_encode(['success' => 0, 'message' => __('Error while getting shape list by manufacturer.', 'tile-tool')]);
        wp_die();
    }

    $return = [];

    $podsField_ManuShapes = pods_field('manufacturer', $_GET['manuID'], 'shape', false);
    if (empty($podsField_ManuShapes)) {
        echo json_encode(['success' => 0, 'message' => __('Error while getting shape and size list by manufacturer.', 'tile-tool')]);
        wp_die();
    } else {
        $shape = $size = $shapeSize = [];
        // get shape by manufacturer only
        foreach ($podsField_ManuShapes as $i => $manuShape) {
            $shape[$manuShape['slug']] = $manuShape['name'];
            // get full shape and size by manufacturer only
            $podsField_ManuFullSizes = pods_field('manufacturer', $_GET['manuID'], 'supported_' . $manuShape['slug'] . '_sizes', false);
            foreach ($podsField_ManuFullSizes as $i => $_size) {
                $shapeSize[$manuShape['slug']][$_size['slug']] = $_size['name'];
            }
        }
        // get size by manufacturer only
        $podsField_ManuSizes = pods_field('manufacturer', $_GET['manuID'], 'supported_' . $_GET['shapeSlug'] . '_sizes', false);
        foreach ($podsField_ManuSizes as $i => $manuSize) {
            $size[$manuSize['slug']] = $manuSize['name'];
        }

        $return['shape'] = $shape;
        $return['size'] = $size;
        $return['shapeSize'] = $shapeSize;
    }
    echo json_encode(['success' => 1, 'message' => $return]);
    wp_die();
}

function getShapeByName($shapeName)
{
    return get_term_by('name', $shapeName, 'pa_shape');
}

function getCategories()
{
    return get_terms([
        'taxonomy' => 'product_cat',
        'hide_empty' => false
    ]);
}

function getCatByName($catName)
{
    return get_term_by('name', $catName, 'product_cat');
}

function getCatById($catID)
{
    return get_term_by('id', $catID, 'product_cat');
}

function getSizeByShapeSlug($shapeSlug)
{
    $shapeSize = get_terms([
        'taxonomy' => 'pa_' . $shapeSlug . '-size',
        'fields' => 'id=>name',
        'hide_empty' => false
    ]);

    return json_encode($shapeSize);
}

function getSizeByName($sizeName, $taxonomy)
{
    return get_term_by('name', $sizeName, 'pa_' . $taxonomy);
}

function getRelatedProducts($productID)
{
    $relatedCate = wp_get_post_terms($productID, 'product_cat');
    $product_cat = [];
    foreach ($relatedCate as $cat) {
        $product_cat[] = $cat->term_id;
    }
    $relatedTag = wp_get_post_terms($productID, 'product_tag');
    $product_tag = [];
    foreach ($relatedTag as $tag) {
        $product_tag[] = $tag->term_id;
    }

    $relatedProducts = get_posts([
        'posts_per_page' => 6,
        'post_type' => 'product',
        'post_status' => 'publish',
        'tax_query' => [
            'relation' => 'OR',
            [
                'taxonomy'  => 'product_cat',
                'field'     => 'term_id',
                'terms'     => $product_cat,
                'include_children' => false
            ],
            [
                'taxonomy'  => 'product_tag',
                'field'     => 'term_id',
                'terms'     => $product_tag,
                'include_children' => false
            ]
        ]
    ]);
    $returnData = [];
    foreach ($relatedProducts as $index => $relatedProduct) {
        $podsField_ProductDesignType = pods_field('product', $relatedProduct->ID, 'product_design_type', true);
        if ($podsField_ProductDesignType == 'fixed') {
            $selectedShape = wc_get_product_terms($relatedProduct->ID, 'pa_shape', [
                'fields' => 'all'
            ]);
            $selectedSize = wc_get_product_terms($relatedProduct->ID, 'pa_' . $selectedShape[0]->slug . '-size', [
                'fields' => 'all'
            ]);
            $_relatedCate = wp_get_post_terms($relatedProduct->ID, 'product_cat');
            $product_cat = [];
            foreach ($_relatedCate as $cat) {
                $product_cat[] = $cat->term_id;
            }
            $_random_cat = array_rand($product_cat, 1);
            $detailLink = get_permalink(get_page_by_path('tile-tool'))
                . '?action=productDetail'
                . '&shapeSlug=' . $selectedShape[0]->slug
                . '&sizeID=' . $selectedSize[0]->term_id
                . '&catID=' . $product_cat[$_random_cat]
                . '&productID=' . $relatedProduct->ID;
            $returnData[$relatedProduct->ID] = [
                'title' => $relatedProduct->post_title,
                'imgSrc' => get_the_post_thumbnail_url($relatedProduct->ID, 'full'),
                'detailLink' => $detailLink
            ];
        }
    }

    foreach ($returnData as $id => $data) {
        if ($id == $productID || !checkPermissionIfProductDefault($id)) {
            unset($returnData[$id]);
            break;
        }
    }

    return $returnData;
}

function getProducts($shapeSlug, $sizeId, $catSlug, $page)
{
    $args = [
        'posts_per_page' => 3000, 
        'paged' => $page,
        'post_type' => 'product',
        'post_status' => 'publish',
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy'  => 'pa_' . $shapeSlug . '-size',
                'field'     => 'term_id',
                'terms'     => [$sizeId],
                'include_children' => false
            ],
            [
                'taxonomy'  => 'pa_shape',
                'field'     => 'slug',
                'terms'     => [$shapeSlug],
                'include_children' => false
            ],
            [
                'taxonomy'  => 'product_cat',
                'field'     => 'slug',
                'terms'     => [$catSlug],
                'include_children' => false
            ]
        ]
    ];
    $products = get_posts($args);
    wp_reset_query();
    $wp_query = new WP_Query($args);
    $end = ($page > 0 && $wp_query->max_num_pages < $page) ? 1 : 0;

    if (!empty($products)) {
        foreach ($products as $index => $product) {
            if (!checkPermissionIfProductDefault((int) $product->ID)) {
                unset($products[$index]);
                continue;
            }
            $products[$index]->thumbImg = get_the_post_thumbnail_url($product->ID);
        }
    }

    $return = ['products' => $products, 'page' => $page, 'end' => $end];

    return json_encode($return);
}

function _getProductDetail($productID)
{
    $woocommerce = WooAPI::getClient();
    if ($woocommerce instanceof Exception) {
        return null;
    }
    $productDetail = $woocommerce->get('products/' . $productID);

    if (empty($productDetail) || $productDetail['product']['status'] !== 'publish') return [];

    // Get the Pods object for a specific item. Pod is used to store uploaded layers in More field Box inside Product editor page
    $podsField_ProductLayers = pods_field('product', $productID, 'product_layer', false); //pods('product', 'layers');
    if (empty($podsField_ProductLayers)) $podsField_ProductLayers = [];

    $podsField_ProductConfigData = pods_field('product', $productID, 'product_config_data', true);
    if (empty($podsField_ProductConfigData)) $podsField_ProductConfigData = json_encode([]);

    $podsField_ProductDesignType = pods_field('product', $productID, 'product_design_type', true);
    if (empty($podsField_ProductDesignType)) $podsField_ProductDesignType = json_encode([]);

    $podsField_ProductManufacturer = pods_field('product', $productID, 'product_manufacturer', true);
    if (empty($podsField_ProductManufacturer)) $podsField_ProductManufacturer = json_encode([]);

    $layerList = [];
    foreach ($podsField_ProductLayers as $layer) {
        $layerList[$layer['ID']] = [
            'src' => get_the_post_thumbnail_url($layer['ID'], 'full'),
            'name' => $layer['post_title']
        ];
    }

    $productPriceObject = json_encode([]);
    switch ($podsField_ProductDesignType) {
        case 'fixed':
            $productPriceObject = pods_field('product', $productID, 'product_price_object', true);
            if (!empty($productPriceObject)) $productPriceObject = json_decode($productPriceObject, true);
            break;
        case 'customized':
            $productPriceObject = get_option('customized-price-object');
            break;
    }

    $productDetail['product']['layers'] = $layerList;
    $productDetail['product']['configData'] = json_decode($podsField_ProductConfigData, true);
    $productDetail['product']['priceObject'] = $productPriceObject;
    $productDetail['product']['productDesignType'] = $podsField_ProductDesignType;
    $productDetail['product']['productManufacturer'] = (is_array($podsField_ProductManufacturer) && isset($podsField_ProductManufacturer["ID"])) ? $podsField_ProductManufacturer["ID"] : '';
    return $productDetail['product'];
}

// http://tileshop.local/wp-admin/admin-ajax.php?action=getSizeByShape&slug=hexagon
function getSizeByShape()
{
    $_shapeSlug = $_GET['slug'];

    echo getSizeByShapeSlug($_shapeSlug);

    wp_die(); // this is required to terminate immediately and return a proper response
}

//	http://tileshop.local/tile-tool/?action=productDetail&productID=13&shapeSlug=hexagon&sizeID=20&catID=7
function getProductsByFilter()
{
    $_shapeSlug = $_GET['shapeSlug'];
    $_sizeId = $_GET['sizeId'];
    $_catSlug = $_GET['catSlug'];
    $_page = empty($_GET['page']) ? 1 : $_GET['page'];

    echo getProducts($_shapeSlug, $_sizeId, $_catSlug, $_page);

    wp_die(); // this is required to terminate immediately and return a proper response
}

function makeLink()
{
    if (empty($_POST)) echo json_encode(['success' => 0, 'message' => '']);
    $data = (!empty($_POST['layerCanvas']) && $_POST['fromProductDetail']) ? $_POST['layerCanvas'] : [];
    $current_user = null;
    if (!is_user_logged_in()) {
        $sID = session_id();
        $_SESSION[$sID] = !empty($_SESSION[$sID] && $_SESSION[$sID] == $data) ? $_SESSION[$sID] : $data;
    } else {
        $current_user = wp_get_current_user();
        $_SESSION[$current_user->user_login] = (!empty($_SESSION[$current_user->user_login]) && $_SESSION[$current_user->user_login] == $data) ? $_SESSION[$current_user->user_login] : $data;
    }

    $link = '';
    if (!empty($_POST['toFactory']) && $_POST['toFactory']) {
        $link = str_replace(['&toFactory=1', '&toShare=1'], '', $_SERVER['HTTP_REFERER'])  . '&toFactory=1';
    } elseif (!empty($_POST['toShare']) && $_POST['toShare'] && is_user_logged_in()) {

        $productDetail = $_POST['productDetail'];

        global $wpdb;
        $wpdb->insert(
            $wpdb->prefix . 'tool_share_links',
            [
                'id' => null,
                'customizedData' => json_encode($data),
                'productID' => $productDetail['productID'],
                'shapeSlug' => $productDetail['shapeSlug'],
                'catID' => $productDetail['catID'],
                'sizeID' => $productDetail['sizeID'],
                'shortLink' => $_SERVER['HTTP_REFERER'],
                'thickness' => $productDetail['thickness'],
                'terazo' => $productDetail['terazo'],
                'created' => current_time('mysql'),
                'updated' => current_time('mysql')
            ]
        );
        $link = $_POST['link'] . '?action=shareLink&id=' . $wpdb->insert_id;
    } else {
        echo json_encode(['success' => 0, 'message' => __('You cannot make a share link. Please login before use share link.', 'tile-tool')]);
        wp_die();
    }

    echo json_encode(['success' => 1, 'link' => $link]);
    wp_die();
}

function getTileToolShareLink($shareID, $selectCols = [])
{
    $selectCols = (!empty($selectCols) && is_array($selectCols)) ? implode(',', $selectCols) : '*';

    global $wpdb;
    $result = $wpdb->get_row($wpdb->prepare(
        "SELECT " . $selectCols . " FROM " . ($wpdb->prefix . 'tool_share_links') . " WHERE id=%d",
        $shareID
    ));
    return $result;
}

function addToWishlist()
{
    if (!is_user_logged_in()) {
        $return = json_encode(['success' => 0, 'message' => __('Please login first for using this action.', 'tile-tool')]);
    } else {
        if ($_POST['action'] !== 'addToWishlist' && (empty($_POST['layerCanvas']) || empty($_POST['productDetail']))) {
            $return = json_encode(['success' => 0, 'message' => 'Error while adding product to wish list']);
        } else {
            $productDetail = $_POST['productDetail'];
            $data = $_POST['layerCanvas'];

            global $wpdb;
            $wpdb->insert($wpdb->prefix . 'tool_share_links', [
                'id' => null,
                'customizedData' => json_encode($data),
                'productID' => $productDetail['productID'],
                'shapeSlug' => $productDetail['shapeSlug'],
                'catID' => $productDetail['catID'],
                'sizeID' => $productDetail['sizeID'],
                'shortLink' => $_SERVER['HTTP_REFERER'],
                'thickness' => $productDetail['thickness'],
                'terazo' => $productDetail['terazo'],
                'image_data' => $productDetail['image'],
                'user_id' => wp_get_current_user()->ID,
                'created' => current_time('mysql'),
                'updated' => current_time('mysql')
            ]);
            $return = json_encode(['success' => 1, 'message' => __('Item was added successful.', 'tile-tool')]);
        }
    }
    echo $return;
    wp_die();
}

function addToCart()
{
    if (empty($_POST)) echo json_encode(['success' => 0, 'message' => '']);
    $customCheckoutID = null;
    $_SESSION['customCheckout'] = !empty($_SESSION['customCheckout']) ? $_SESSION['customCheckout'] : [];
    $sID = !is_user_logged_in() ? session_id() : wp_get_current_user()->user_login;
    $productID = $_POST['productDetail']['productID'];
    $productHash = md5($productID . $sID . time());
    $sID = substr($sID, 0, 6);
    $productHash = substr($productHash, 0, 6);
    $cartLink = home_url('/tile-tool') . '?action=editCartItem&orderId=' . $sID;
    $cartLink .= '&itemId=' . $productHash;
    $_SESSION['customCheckout'][$sID][$productHash] = $_POST;

    echo json_encode([
        'success' => 1,
        'message' => __('Your cart was added new item successful.', 'tile-tool'),
        'redirectLink' => $cartLink
    ]);
    wp_die();
}

function getTilesShopCheckout($customCheckoutID)
{
    if (empty($customCheckoutID) || empty($_SESSION['customCheckout']) || empty($_SESSION['customCheckout'][$customCheckoutID])) {
        return null;
    }

    $cartItems = $_SESSION['customCheckout'][$customCheckoutID];
    foreach ($cartItems as $id => $item) {
        $imageData = $item['image'];
        $imageName = 'order_image_of_' .  $customCheckoutID . '_' . $id;
        if (empty($item['imageLink'])) {
            $image = uploadFeaturedImageForProduct(null,  $imageName, $imageData, false);
            $cartItems[$id]['imageLink'] =  $image[0];
        }
    }
    $_SESSION['customCheckout'][$customCheckoutID] = $cartItems;

    return $_SESSION['customCheckout'][$customCheckoutID];
}

function getAllShippingMethodByZoneId($zoneID)
{
    $shippingZones = listAllShippingZones();
    $shippingMethods = [];
    foreach ($shippingZones as $i => $zone) {
        if ($zoneID == $i) {
            foreach ($zone['shipping_methods'] as $k => $method) {
                if ($method->enabled == 'yes') {
                    $methodData = ['id' => $k, 'cost' => $method->cost];
                    switch ($method->title) {
                        case 'BasicBox':
                            $shippingMethods['basicBox'] = $methodData;
                            break;
                        case 'Box_1sqm':
                            $shippingMethods['box1sqm'] = $methodData;
                            break;
                        case 'BasicPallet':
                            $shippingMethods['basicPallet'] = $methodData;
                            break;
                        case 'Pallet_1sqm':
                            $shippingMethods['pallet1sqm'] = $methodData;
                            break;
                    }
                }
            }
            break;
        }
    }

    return $shippingMethods;
}

function listAllShippingZones()
{
    $shippingZones = WC_Shipping_Zones::get_zones();
    if (!empty($shippingZones)) return $shippingZones;
    return false;
}

function getShippingZonesByCountry()
{
    if (empty($_GET)) {
        echo json_encode(['success' => 0, 'message' => '']);
    } else {
        $g_action = (!empty($_GET['action']) && $_GET['action'] === 'getShippingZonesByCountry') ? $_GET['action'] : '';
        $g_formatted_zone_location = !empty($_GET['formatted_zone_location']) ? $_GET['formatted_zone_location'] : '';
        if (empty($g_action) || empty($g_formatted_zone_location)) echo json_encode(['success' => 0, 'message' => '']);

        $shippingZones = listAllShippingZones();
        $result = [];
        foreach ($shippingZones as $i => $shippingZone) {
            if ($shippingZone['formatted_zone_location'] == $g_formatted_zone_location) {
                $result[$shippingZone['id']] = $shippingZone['zone_name'];
            }
        }

        echo json_encode(['success' => 1, 'message' => $result]);
    }

    wp_die();
}

function calTotalBill()
{
    $message = [];
    $g_action = (!empty($_GET['action']) && $_GET['action'] == 'calTotalBill') ? $_GET['action'] : '';
    $p_data = !empty($_POST['data']) ? $_POST['data'] : [];
    if ($g_action !== '' && !empty($p_data)) {
        $_shippingZone = $p_data['shippingZone'];
        $shippingMethods = getAllShippingMethodByZoneId($_shippingZone);
        $_shippingCountryName = $p_data['shippingCountryName'];
        $tax = get_posts([
            'post_type' => 'country_tax',
            'post_status' => 'publish',
            'post_title' => $_shippingCountryName
        ]);
        $tax = pods_field('country_tax', $tax[0]->ID, 'tax-percent', true);

        $totalPieces = $totalBoxes = $totalPallet = $shippingFee = $taxFee = $orderTotal = $totalBill = 0;
        $ppa = $ppb = $bpp = $orderedArea = $orderedPricing = $areaPerBox = $areaPerPallet = 0;
        $prodDetailBill = [];
        foreach ($p_data['data'] as $i => $data) {
            $_orderProdKey = $data['orderProdKey'];
            $_orderedArea = $data['orderedArea'];
            $_orderedPricing = $data['orderedPricing'];
            $_ppa = $data['ppa'];
            $_ppb = $data['ppb'];
            $_bpp = $data['bpp'];

            $_totalPieces = $_orderedArea * $_ppa;
            $_totalBoxes = $_totalPieces / $_ppb;
            $_totalPallet = $_totalBoxes / $_bpp;

            $orderedArea += $_orderedArea;
            $orderedPricing += $_orderedPricing;

            $ppa += $_ppa;
            $ppb += $_ppb;
            $bpp += $_bpp;

            $totalPieces += $_totalPieces;
            $totalBoxes += $_totalBoxes;
            $totalPallet += $_totalPallet;

            $_areaPerBox = $_ppb / $_ppa;
            $areaPerBox += $_areaPerBox;

            $_areaPerPallet = $_areaPerBox * $_bpp;
            $areaPerPallet += $_areaPerPallet;

            if ($_orderedArea >= $_areaPerPallet) {
                // Enough for packaging from 1 and more pallet
                // Then calculate based on pallet shipping fee
                $numberOfPallet = $_orderedArea / $_areaPerPallet;
                $fractionalPart = fmod($numberOfPallet, 1);
                $fractionalPartToArea = ($fractionalPart * $_bpp * $_ppb) / $_ppa;
                $_shippingFee = (floor($numberOfPallet) * $shippingMethods['basicPallet']['cost']) + ($fractionalPartToArea * $shippingMethods['pallet1sqm']['cost']);
            } else {
                // Enough for packaging in boxes
                // Then calculate based on box shipping fee
                $numberOfBox = $_orderedArea / $_areaPerBox;
                $fractionalPart = fmod($numberOfBox, 1);
                $fractionalPartToArea = ($fractionalPart * $_ppb) / $_ppa;
                $_shippingFee = (floor($numberOfBox) * $shippingMethods['basicBox']['cost']) + ($fractionalPartToArea * $shippingMethods['box1sqm']['cost']);
            }

            $_shippingFee = wc_format_decimal($_shippingFee, 2);
            $_totalBill = $_shippingFee + floatval($_orderedPricing);
            $shippingFee += $_shippingFee;

            $totalBill += $_totalBill;

            $_taxFee = $_totalBill * intval($tax) / 100;
            $_orderTotal = $_totalBill + $_taxFee;

            $prodDetailBill[$_orderProdKey] = wc_format_decimal($_orderTotal, 2);

            $taxFee += $_taxFee;
            $orderTotal += $_orderTotal;
        }

        $message = [
            'totalPieces' => wc_format_decimal($totalPieces, 0),
            'totalBoxes' => wc_format_decimal($totalBoxes, 2),
            'totalPallet' => wc_format_decimal($totalPallet, 2),
            'shippingFee' => $shippingFee,
            'totalBill' => wc_format_decimal($totalBill, 2),
            'tax' => $tax,
            'taxFee' => wc_format_decimal($taxFee, 2),
            'orderTotal' => wc_format_decimal($orderTotal, 2),
            'prodDetailBill' => $prodDetailBill
        ];
    }

    echo json_encode(['success' => !empty($message) ? 1 : 0, 'message' => $message]);
    wp_die();
}

function placeOrder()
{
    $message = [];
    $g_action = (!empty($_GET['action']) && $_GET['action'] == 'placeOrder') ? $_GET['action'] : '';
    $p_data = !empty($_POST['data']) ? $_POST['data'] : [];
    if ($g_action !== '' && !empty($p_data)) {
        $woocommerce = WooAPI::getClient();
        if ($woocommerce instanceof Exception) {
            echo json_encode(['success' => 0, 'message' => $woocommerce->getMessage()]);
            wp_die();
        }

        $paymentMethod = $p_data['paymentMethod'];
        $data = [
            'order' => [
                'customer_id' => $p_data['customerID'],
                'billing_address' => [
                    'first_name' => $p_data['billing']['firstName'],
                    'last_name' => $p_data['billing']['lastName'],
                    'address_1' => $p_data['billing']['company'],
                    'city' => $p_data['billing']['city'],
                    'postcode' => $p_data['billing']['postcode'],
                    'email' => $p_data['billing']['email'],
                    'phone' => $p_data['billing']['phone']
                ],
                'shipping_address' => [
                    'first_name' => $p_data['billing']['firstName'],
                    'last_name' => $p_data['billing']['lastName'],
                    'address_1' => $p_data['billing']['company'],
                    'city' => $p_data['billing']['city'],
                    'postcode' => $p_data['billing']['postcode'],
                    'email' => $p_data['billing']['email'],
                    'phone' => $p_data['billing']['phone']
                ],
                'line_items' => []
            ]
        ];

        foreach ($p_data['orderedProduct'] as $i => $item) {
            $data['order']['line_items'][] = [
                'product_id' => $item['productID'],
                'quantity' => 1,
                'total' => $item['productDetail']['total']
            ];
        }
        $order = [];
        try {
            $order = $woocommerce->post('orders', $data);
            if (!empty($order)) {
                // Set shareLink and shippingInformation for order detail
                $shareLink = [];
                global $wpdb;
                foreach ($p_data['orderedProduct'] as $i => $item) {
                    $wpdb->insert(
                        $wpdb->prefix . 'tool_share_links',
                        [
                            'id' => null,
                            'customizedData' => stripslashes($item['productDetail']['customizedData']),
                            'productID' => $item['productID'],
                            'shapeSlug' => $item['productDetail']['shapeSlug'],
                            'catID' => $item['productDetail']['catID'],
                            'sizeID' => $item['productDetail']['sizeID'],
                            'thickness' => $item['productDetail']['thickness'],
                            'shortLink' => '',
                            'terazo' => $item['productDetail']['terazo'],
                            'created' => current_time('mysql'),
                            'updated' => current_time('mysql')
                        ]
                    );
                    $shareLink[] = get_permalink(get_page_by_path('tile-tool')) . '?action=shareLink&id=' . $wpdb->insert_id;
                }

                $shop_order = pods('shop_order');
                $shop_order->save([
                    'share_link' => $shareLink,
                    'shipping_information' => _get_shipping_information_view($data['order']['shipping_address']),
                    'order_information' => _get_order_information_view($p_data)
                ], null, $order['order']['id']);
                if (!empty($p_data['billing']['notes'])) {
                    $woocommerce->post('orders/' . $order['order']['id'] . '/notes', [
                        'order_note' => [
                            'note' => $p_data['billing']['notes'],
                            'customer_note' => true
                        ]
                    ]);
                }

                // Store Order ID in session so it can be re-used after payment failure
                WC()->session->order_awaiting_payment = $order['order']['id'];

                // Process Payment
                $available_gateways = WC()->payment_gateways->get_available_payment_gateways();
                $result = $available_gateways[$paymentMethod]->process_payment($order['order']['id']);

                // Redirect to success/confirmation/payment page
                if ($result['result'] == 'success') {

                    $result = apply_filters('woocommerce_payment_successful_result', $result, $order['order']['id']);
                    $message = $result['redirect'];
                    unset($_SESSION['customCheckout']);
                    //                    WC()->session->order_awaiting_payment = null;
                }
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
        }
    }
    echo json_encode(['success' => 1, 'message' => $message]);
    wp_die();
}

function _get_order_information_view($data)
{
    $html = '<div class="row">';

    foreach ($data['orderedProduct'] as $idx => $prod) {
        $html .= '<div class="col-sm-2">';
        $html .= '<label><strong>Product ' . ++$idx . ': ' . $prod['productDetail']['name'] . ': </strong></label> $' . $prod['productDetail']['price'];
        $html .= '</div>';
    }

    $html .= '<div class="col-sm-2">';
    $html .= '<label>';
    $html .= '<strong>Shipping Detail Price:</strong><br>' . $data['order']['shipping_detail'];
    $html .= '</label>';
    $html .= '</div>';

    $html .= '<div class="col-sm-2">';
    $html .= '<label>';
    $html .= '<strong>Total Bill:</strong> $' . $data['order']['total_bill'];
    $html .= '</label>';
    $html .= '</div>';

    $html .= '<div class="col-sm-2">';
    $html .= '<label>';
    $html .= '<strong>Tax:</strong>' . $data['order']['total_tax'] . '% ~ $' . number_format(floatval($data['order']['total_bill']) * floatval($data['order']['total_tax']) / 100, 2);
    $html .= '</label>';
    $html .= '</div>';

    $html .= '<div class="col-sm-2">';
    $html .= '<label>';
    $html .= '<strong>Order Total:</strong> $' . $data['order']['total'];
    $html .= '</label>';
    $html .= '</div>';

    $html .= '</div>';

    return $html;
}

function _get_shipping_information_view($shippingInformation)
{
    $html = '<div class="row">';
    foreach ($shippingInformation as $name => $value) {
        $html .= '<div class="col-sm-2">';
        $html .= '<label><strong>' . $name . ':</strong></label> ' . $value;
        $html .= '</div>';
    }
    $html .= '</div>';

    return $html;
}

function _upsertProduct(
    $upsertType,
    $defaultProductID,
    $data,
    $newProductName,
    $categories = null,
    $shapeAndSize = null,
    $featuredImage = null,
    $priceObject = null,
    $minOrder = null,
    $thicknesses = null,
    $delivery = null,
    $packaging = null,
    $customDesign = null,
    $weight = null,
    $productDesignType,
    $productManufacturer
) {
    if (empty($upsertType) || !in_array($upsertType, ['create', 'update'])) return [];
    // GET TEMP PRODUCT
    try {
        $woocommerce = new \Automattic\WooCommerce\Client(
            esc_url(home_url('/')),
            API_KEY,
            API_SEC,
            [
                'verify_ssl' => false
            ]
        );
        $productDefault = $woocommerce->get('products/' . $defaultProductID);
    } catch (Exception $ex) {
        return null;
    }
    $newProductName = stripslashes_deep($newProductName);

    $_productLayers = [];
    foreach ($data as $layerName => $layerData) {
        if (!empty($layerData['id']) && $layerName !== 'background-layer' && $layerData['duplicateID'] == 0) {
            $_productLayers[] = $layerData['id'];
        }
    }

    $_shapes = [];
    foreach ($shapeAndSize as $shape => $shapeData) {
        $_shapes['slug'][] = $shape;
        $_shapes['sizeSlug'][] = $shape . '-size';
    }

    $defaultAttributes = [];
    foreach ($productDefault['product']['attributes'] as $index => $at) {
        $defaultAttributes[$at['slug']] = $at['options'];
    }

    $langList = pll_languages_list();
    $currentLang = pll_current_language();
    $langCounter = count($langList);
    $catList = [];
    $relatedTranslation = [];
    $relatedTranslationUpsertType = [];
    $categories = !empty($categories) ? $categories : $productDefault['product']['categories'];
    for ($langIdx = 0; $langIdx < $langCounter; $langIdx++) {
        if ($langList[$langIdx] != $currentLang || $currentLang != pll_get_post_language($defaultProductID)) {
            $tmpTranslatedProduct = pll_get_post($defaultProductID, $langList[$langIdx]);
            if (empty($tmpTranslatedProduct) || get_post_status($tmpTranslatedProduct) === 'trash') {
                $relatedTranslationUpsertType[$langList[$langIdx]] = [
                    'upsertType' => 'create'
                ];
            } else {
                $relatedTranslationUpsertType[$langList[$langIdx]] = [
                    'productID' => $tmpTranslatedProduct,
                    'upsertType' => 'update'
                ];
            }
        } else {
            $relatedTranslationUpsertType[$langList[$langIdx]] = [
                'productID' => $defaultProductID,
                'upsertType' => 'update'
            ];
        }
        $cat[$langList[$langIdx]] = [];
        foreach ($categories as $idx => $catId) {
            $tmpCat = get_term_by('id', $catId, 'product_cat');
            $tmpCatSlug = explode('-', $tmpCat->slug);
            if ($langList[$langIdx] != 'en') {
                $tmpTerm = get_terms([
                    'taxonomy' => 'product_cat',
                    'lang' => $langList[$langIdx],
                    'slug' => $tmpCatSlug[0] . '-' . $langList[$langIdx],
                    'hide_empty' => false
                ]);
            } else {
                $tmpTerm = get_terms([
                    'taxonomy' => 'product_cat',
                    'lang' => 'en',
                    'slug' => $tmpCatSlug[0],
                    'hide_empty' => false
                ]);
            }
            $catList[$langList[$langIdx]][] = $tmpTerm[0]->name;
        }
    }

    try {
        for ($langIdx = 0; $langIdx < $langCounter; $langIdx++) {
            if ($upsertType === 'create' || $relatedTranslationUpsertType[$langList[$langIdx]]['upsertType'] === 'create') {
                $tmpProduct = $woocommerce->post('products', $productDefault);
            } else {
                $tmpProduct = $productDefault;
                if ($langList[$langIdx] != $currentLang || $currentLang != pll_get_post_language($defaultProductID)) {
                    $tmpProduct = $woocommerce->get('products/' . $relatedTranslationUpsertType[$langList[$langIdx]]['productID']);
                }
            }
            // Set categories for new product
            wp_set_object_terms($tmpProduct['product']['id'], $catList[$langList[$langIdx]], 'product_cat');
            // Set shape and size for new product
            if (!empty($shapeAndSize)) {
                // Get default attributes and build new attributes for new product
                $newProductAttributes = [];
                $productAttributes = get_post_meta($productDefault['product']['id'], '_product_attributes');
                foreach ($productAttributes[0] as $i => $attData) {
                    $_slug = str_replace('pa_', '', $attData['name']);
                    if (preg_match('/-size/', $attData['name'])) continue;

                    wp_set_object_terms($productDefault['product']['id'], $defaultAttributes[$_slug], $attData['name']);
                    $newProductAttributes[] = [
                        'name' => $attData['name'],
                        'value' => ($attData['name'] === 'pa_minimum-order' ? [$minOrder] : $defaultAttributes[$_slug]),
                        'is_visible' => '1',
                        'is_taxonomy' => '1'
                    ];
                }
                // Build size attributes for new product
                foreach ($shapeAndSize as $shape => $shapeData) {
                    wp_set_object_terms($tmpProduct['product']['id'], $shapeData['size'], 'pa_' . $shape . '-size');
                    $newProductAttributes[] = [
                        'name' => 'pa_' . $shape . '-size',
                        'value' => $shapeData['size'],
                        'is_visible' => '1',
                        'is_taxonomy' => '1'
                    ];

                    $newProductAttributes[] = [
                        'name' => 'pa_shape',
                        'value' => $shape,
                        'is_visible' => '1',
                        'is_taxonomy' => '1'
                    ];
                }
                // Update new attributes for new product
                update_post_meta($tmpProduct['product']['id'], '_product_attributes', $newProductAttributes);
                // Set shape for new product
                wp_set_object_terms($tmpProduct['product']['id'], $_shapes['slug'], 'pa_shape');
            }

            // Set thickness for new product
            if (!empty($thicknesses)) {
                wp_set_object_terms($tmpProduct['product']['id'], $thicknesses, 'pa_thickness');
            }

            // Set delivery for new product
            if (!empty($delivery)) {
                wp_set_object_terms($tmpProduct['product']['id'], stripslashes_deep($delivery), 'pa_delivery-time');
            }

            if (!empty($packaging)) {
                wp_set_object_terms($tmpProduct['product']['id'], stripslashes_deep($packaging), 'pa_packaging');
            }

            if (!empty($customDesign)) {
                wp_set_object_terms($tmpProduct['product']['id'], stripslashes_deep($customDesign), 'pa_custom-design');
            }

            // Set weight of tile for new product
            if (!empty($weight)) {
                wp_set_object_terms($tmpProduct['product']['id'], $weight, 'pa_weight-per-piece');
            }

            // Set pieces of m2 for new product
            if (!empty($pieceOfM2)) {
                wp_set_object_terms($tmpProduct['product']['id'], $pieceOfM2, 'pa_piece-of-m2');
            }

            if (!empty($minOrder)) {
                $minOrderTerm = [
                    'name' => $minOrder
                ];
                wp_set_object_terms($tmpProduct['product']['id'], $minOrderTerm, 'pa_minimum-order');
            }

            //        $podsField_ProductConfigData = pods_field ( 'product', $tmpProduct['product']['id'], 'product_config_data', true );
            //        if($upsertType != 'create'
            //            && (stripslashes_deep(json_encode($data)) == stripslashes_deep($podsField_ProductConfigData))
            //            && !empty($tmpProduct['product']['featured_src'])) {
            //            $featuredImage = null;
            //        }

            // Update featured image into new product
            if (!empty($featuredImage)) {
                uploadFeaturedImageForProduct($tmpProduct['product']['id'], $newProductName, $featuredImage, true);
            }

            $priceObject = !is_null($priceObject) ? $priceObject : [];
            if (!empty($priceObject)) {
                ksort($priceObject['priceAreaPair']);
            }

            // Set layers and config data for new product
            $myPods = pods('product');
            $myPods->add_to('related_item_id', $_productLayers);
            $myPods->add_to('related_item_id', $productManufacturer);
            $myPods->save([
                'product_layer' => $_productLayers,
                'product_config_data' => json_encode($data),
                'product_price_object' => json_encode($priceObject),
                'product_design_type' => $productDesignType,
                'product_manufacturer' => $productManufacturer
            ], null, $tmpProduct['product']['id']);

            // Update the post title into the database
            wp_update_post([
                'ID'           => $tmpProduct['product']['id'],
                'post_title'   => esc_sql($newProductName)
            ]);
            if ($upsertType === 'create' || $relatedTranslationUpsertType[$langList[$langIdx]]['upsertType'] === 'create') {
                pll_set_post_language($tmpProduct['product']['id'], $langList[$langIdx]);
            }
            $relatedTranslation[$langList[$langIdx]] = $tmpProduct['product']['id'];
        }
        pll_save_post_translations($relatedTranslation);
        return true;
    } catch (Exception $e) {
        return false;
    }
}

function uploadFeaturedImageForProduct($productID = null, $imageName, $imageData, $isFeatured = false)
{
    $upload_dir = wp_upload_dir();

    // @new
    $upload_path = str_replace('/', DIRECTORY_SEPARATOR, $upload_dir['path']) . DIRECTORY_SEPARATOR;

    $img = $imageData;
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);

    $decoded = base64_decode($img);
    $hashed_filename  = md5($imageName . microtime()) . '_' . $imageName;
    // @new
    file_put_contents($upload_path . $hashed_filename, $decoded);
    //HANDLE UPLOADED FILE
    if (!function_exists('wp_handle_sideload')) {
        require_once(ABSPATH . 'wp-admin/includes/file.php');
    }

    // Without that I'm getting a debug error!?
    if (!function_exists('wp_get_current_user')) {
        require_once(ABSPATH . 'wp-includes/pluggable.php');
    }

    // @new
    $file             = array();
    $file['error']    = '';
    $file['tmp_name'] = $upload_path . $hashed_filename;
    $file['name']     = $hashed_filename . '.png';
    $file['type']     = 'image/png';
    $file['size']     = filesize($upload_path . $hashed_filename);

    // upload file to server
    // @new use $file instead of $image_upload
    $file_return      = wp_handle_sideload($file, array('test_form' => false));
    $filename = $file_return['file'];

    $attachment = array(
        'post_mime_type' => $file_return['type'],
        'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
        'post_content' => '',
        'post_status' => 'inherit',
        'guid' => $upload_dir['url'] . '/' . basename($filename)
    );
    $attach_id = wp_insert_attachment($attachment, $filename, 289);
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
    wp_update_attachment_metadata($attach_id, $attach_data);
    $image = wp_get_attachment_image_src($attach_id, 'full');
    if ($isFeatured && !is_null($productID) && $image[0]) {
        // determine if in the $media image we created, the string of the URL exists
        // if so, we found our image. set it as thumbnail
        set_post_thumbnail($productID, $attach_id);
        return true;
    }
    return $image;
}

function upsertProduct()
{
    $return = json_encode(['success' => 0, 'message' => '']);
    $type = !empty($_GET['type']) ? $_GET['type'] : null;
    if (!empty($_POST)) {
        $data = !empty($_POST['layerCanvas']) ? $_POST['layerCanvas'] : [];
        $productID = !empty($_POST['productID']) ? $_POST['productID'] : null;
        $categories = !empty($_POST['categories']) ? $_POST['categories'] : null;
        $shapeAndSize = !empty($_POST['shapeAndSize']) ? $_POST['shapeAndSize'] : null;
        $productTitle = !empty($_POST['productTitle']) ? wp_filter_nohtml_kses($_POST['productTitle']) : 'Edit product from product ID#' . $productID;
        $featuredImage = !empty($_POST['featuredImage']) ? $_POST['featuredImage'] : null;
        $priceObject = !empty($_POST['priceObject']) ? $_POST['priceObject'] : null;
        $minOrder = !empty($_POST['minOrder']) ? $_POST['minOrder'] : null;
        $thicknesses = !empty($_POST['thicknesses']) ? $_POST['thicknesses'] : null;
        $delivery = !empty($_POST['delivery']) ? wp_filter_nohtml_kses($_POST['delivery']) : null;
        $packaging = !empty($_POST['packaging']) ? wp_filter_nohtml_kses($_POST['packaging']) : null;
        $customDesign = !empty($_POST['customDesign']) ? wp_filter_nohtml_kses($_POST['customDesign']) : null;
        $weight = !empty($_POST['weight']) ? $_POST['weight'] : null;
        $productDesignType = !empty($_POST['productDesignType']) ? $_POST['productDesignType'] : null;
        $productManufacturer = !empty($_POST['productManufacturer']) ? $_POST['productManufacturer'] : null;

        // UPSERT PRODUCT
        $result = _upsertProduct(
            $type,
            $productID,
            $data,
            $productTitle,
            $categories,
            $shapeAndSize,
            $featuredImage,
            $priceObject,
            $minOrder,
            $thicknesses,
            $delivery,
            $packaging,
            $customDesign,
            $weight,
            $productDesignType,
            $productManufacturer
        );

        if ($result) {
            $return = json_encode([
                'success' => 1,
                'message' => get_site_url(null, 'wp-admin') . "/edit.php?post_type=product"
            ]);
        }
    }
    echo $return;
    wp_die();
}

function layer_title_filter($where, &$wp_query)
{
    global $wpdb;
    $search_term = $wp_query->get('search_prod_title');
    if ($search_term && !empty($search_term)) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql(like_escape($search_term)) . '%\'';
    }
    return $where;
}

function loadMoreLayer()
{
    if (empty($_GET)) {
        echo json_encode(['success' => 0, 'message' => '']);
        wp_die();
    }
    $page = ($_GET['page'] > 1) ? $_GET['page'] : 0;
    $searchString = !empty($_GET['searchString']) ? $_GET['searchString'] : '';
    $isCat = !empty($_GET['isCat']) ? $_GET['isCat'] : '';

    $posts_per_page = 100;
    $args = [
        'post_type' => 'layer',
        'post_status' => 'publish',
        'posts_per_page' => $posts_per_page,
        'paged' => $page,
        'orderby'     => 'ID',
        'order'       => 'ASC'
    ];

    $existedTag = [];
    if (!$isCat && $isCat != 1) {
        $existedTag = get_term_by('name', $searchString, 'layer_tag');
    }

    if (empty($existedTag) && !$isCat) {
        $args['search_prod_title'] = $searchString;
    } elseif (!$isCat) {
        $args['tax_query'] = [
            [
                'taxonomy'         => 'layer_tag',
                'field'            => 'id',
                'terms'            => [$existedTag->term_id]
            ]
        ];
    } else {
        $catArr = explode(',', $searchString);
        $args['tax_query']['relation'] = 'OR';
        foreach ($catArr as $catId) {
            $args['tax_query'][] = [
                'taxonomy'         => 'layer_cat',
                'field'            => 'id',
                'terms'            => $catId
            ];
        }
    }

    add_filter('posts_where', 'layer_title_filter', 10, 2);
    wp_reset_query();
    $wp_query = new WP_Query($args);

    $layers = [];
    $end = ($page > 0 && $wp_query->max_num_pages < $page) ? true : false;
    if ($wp_query->have_posts()) {
        $allLayers = $wp_query->get_posts();
        $all_post_found = $wp_query->found_posts;
        if ($all_post_found > 0 && $all_post_found < $posts_per_page) $end = true;
        foreach ($allLayers as $layer) {
            $layers[$layer->ID] = [
                'src' => get_the_post_thumbnail_url($layer->ID, 'full'),
                'name' => $layer->post_title
            ];
        }
        remove_filter('posts_where', 'layer_title_filter', 10, 2);
    }

    echo json_encode([
        'success' => 1,
        'message' => $layers,
        'end' => $end
    ]);
    wp_die();
}

function loadColorSelectionBlock()
{
    $colorCategories = get_terms(array(
        'taxonomy' => 'color_cat',
        'hide_empty' => false,
    ));
    if (empty($colorCategories)) {
        echo json_encode(['success' => 0, 'message' => '']);
        wp_die();
    }

    $result = [];
    foreach ($colorCategories as $i => $termObject) {
        $result[$termObject->term_id] = $termObject->name;
    }

    echo json_encode([
        'success' => 1,
        'message' => $result
    ]);
    wp_die();
}

function loadColorSelectionList()
{
    $returnData = json_encode([
        'success' => 0,
        'message' => __('This manufacturer do not have any color or color category is missing. Please contact admin for more detail or choose another manufacturer.', 'tile-tool')
    ]);
    if (!empty($_POST) && !empty($_POST['colorCatID']) && !empty($_POST['manuID'])) {
        $colorCatID = $_POST['colorCatID'];
        $manuID = $_POST['manuID'];
        $colorList = get_posts([
            'post_type' => 'layer_color',
            'post_status' => 'publish',
            'meta_key'         => 'color_manufacturer',
            'meta_value'       => $manuID,
            'tax_query' => [
                'relation' => 'AND',
                [
                    'taxonomy'  => 'color_cat',
                    'field'     => 'term_id',
                    'terms'     => [$colorCatID],
                    'include_children' => false
                ]
            ],
        ]);
        $result = [];
        if (!empty($colorList)) {
            $hover_color_default = get_option('color-hover-configuration');
            foreach ($colorList as $index => $color) {
                $hover_color = pods_field('layer_color', $color->ID, 'hover_color', true);
                $main_color = pods_field('layer_color', $color->ID, 'main_color', true);
                $hover_color = !empty($hover_color) ? $hover_color : $hover_color_default;
                $result[] = [
                    'color' => $main_color,
                    'hover_color' => $hover_color
                ];
            }
            $returnData = json_encode([
                'success' => 1,
                'message' => $result
            ]);
        }
    }

    echo $returnData;
    wp_die();
}

function updateManufacturerData()
{
    if (empty($_POST) || empty($_POST['manuID']) || empty($_POST['shapeSlug']) || empty($_POST['sizeSlug'])) {
        echo json_encode(['success' => 0, 'message' => __('Error while updating manufacturer.', 'tile-tool')]);
        wp_die();
    }

    $thickness = preg_replace('/\s+/', '', $_POST['thickness']);
    $thickness = trim($thickness, ',');
    $thickness = explode(',', $thickness);
    foreach ($thickness as $i => $t) {
        $thickness[$i] = preg_replace('/\s+/', '', $t);
    }
    $thickness = array_unique($thickness);

    $where = [
        'manu_id' => $_POST['manuID'],
        'shape_slug' => $_POST['shapeSlug'],
        'size_slug' => $_POST['sizeSlug']
    ];
    $data = [
        'color_unit_price' => $_POST['color_unit_price'],
        'terazo_price' => $_POST['terazo_price'],
        'terazo_link' => $_POST['terazo_link'],
        'base_price' => $_POST['base_price'],
        'thickness' => json_encode($thickness),
        'bpp' => $_POST['bpp'],
        'ppa' => $_POST['ppa'],
        'ppb' => $_POST['ppb'],
        'ppp' => $_POST['ppp'],
        'wpt' => $_POST['wpt'],
        'wpb' => $_POST['wpb'],
        'image_url_of_on_top_shape' => $_POST['image_url_of_on_top_shape'],
        'template_configuration_data' => $_POST['template_configuration_data'],
        'option_1' => $_POST['option_1'],
        'option_2' => $_POST['option_2']
    ];

    try {
        global $wpdb;
        $existed = $wpdb->get_row($wpdb->prepare(
            "SELECT 1 FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d AND shape_slug LIKE %s AND size_slug LIKE %s LIMIT 1",
            $_POST['manuID'],
            $_POST['shapeSlug'],
            $_POST['sizeSlug']
        ));
        if (!empty($existed)) {
            $wpdb->update($wpdb->prefix . 'tool_manufacturer_data', $data, $where);
        } else {
            $wpdb->insert($wpdb->prefix . 'tool_manufacturer_data', $where + $data);
        }

        $relatedSizes = $wpdb->get_results($wpdb->prepare(
            "SELECT * FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d AND shape_slug LIKE %s AND size_slug NOT LIKE %s",
            $_POST['manuID'],
            $_POST['shapeSlug'],
            $_POST['sizeSlug']
        ));
        if (!empty($relatedSizes)) {
            foreach ($relatedSizes as $relatedSizeObj) {
                $where = [
                    'manu_id' => $relatedSizeObj->manu_id,
                    'shape_slug' => $relatedSizeObj->shape_slug,
                    'size_slug' => $relatedSizeObj->size_slug
                ];
                $data = [
                    'image_url_of_on_top_shape' => $_POST['image_url_of_on_top_shape'],
                ];
                $wpdb->update($wpdb->prefix . 'tool_manufacturer_data', $data, $where);
            }
        }
        echo json_encode(['success' => 1, 'message' => 'Data was updated successfully.', 'thickness' => $thickness]);
    } catch (Exception $e) {
        echo json_encode(['success' => 0, 'message' => $e->getMessage()]);
    }
    wp_die();
}

function removeOrderItem()
{
    $return = json_encode(['success' => 0, 'message' => __('Remove order item out of cart had error.', 'tile-tool')]);
    if (!empty($_POST) && !empty($_POST['itemOrderId']) && !empty($_POST['customCheckoutID'])) {
        $itemOrderId = $_POST['itemOrderId'];
        $customCheckoutID = $_POST['customCheckoutID'];
        try {
            unset($_SESSION['customCheckout'][$customCheckoutID][$itemOrderId]);
            $return = json_encode([
                'success' => 1,
                'message' => 'Remove order item out of cart successful.'
            ]);
        } catch (Exception $e) {
            $return['message'] = $e->getMessage();
        }
    }
    echo $return;
    wp_die();
}

function editOrderItem()
{
    $return = json_encode(['success' => 0, 'message' => 'Edit order item had error.']);
    if (!empty($_POST) && !empty($_POST['orderItemId']) && !empty($_POST['orderId'])) {
        try {
            $orderItemId = $_POST['orderItemId'];
            $orderId = $_POST['orderId'];
            $_SESSION['customCheckout'] = !empty($_SESSION['customCheckout']) ? $_SESSION['customCheckout'] : [];
            if (empty($_SESSION['customCheckout'][$orderId]) || empty($_SESSION['customCheckout'][$orderId][$orderItemId])) {
                throw new Requests_Exception_HTTP_404('Order item or order is not found.');
            }
            unset($_POST['orderItemId']);
            unset($_POST['orderId']);
            $_SESSION['customCheckout'][$orderId][$orderItemId] = $_POST;

            $return = json_encode([
                'success' => 1,
                'message' => 'Edit order item successful.'
            ]);
        } catch (Exception $e) {
            $return['message'] = $e->getMessage();
        }
    }

    echo $return;
    wp_die();
}

function removeWishlistItem()
{
    if (!is_user_logged_in()) {
        $return = json_encode(['success' => 0, 'message' => __('Please login first for using this action.', 'tile-tool')]);
    } else {
        $return = json_encode(['success' => 0, 'message' => __('Remove wishlist item had error.', 'tile-tool')]);
        if (!empty($_POST) && !empty($_POST['itemID'])) {
            $itemID = $_POST['itemID'];
            try {
                global $wpdb;
                $wpdb->delete($wpdb->prefix . 'tool_share_links', [
                    'id' => $itemID
                ]);
                $return = json_encode(
                    [
                        'success' => 1,
                        'message' => __('Remove wishlist item successful.', 'tile-tool')
                    ]
                );
            } catch (Exception $e) {
                $return['message'] = $e->getMessage();
            }
        }
    }
    echo $return;
    wp_die();
}
