<?php

function getProductDetailById($productID)
{
    $woocommerce = WooAPI::getClient();
    if ($woocommerce instanceof Exception) {
        return null;
    }
    $productDetail = $woocommerce->get('products/' . $productID);

    if (empty($productDetail) || $productDetail['product']['status'] !== 'publish') return [];

    // Get the Pods object for a specific item. Pod is used to store uploaded layers in More field Box inside Product editor page
    $podsField_ProductLayers = pods_field('product', $productID, 'product_layer', false); //pods('product', 'layers');
    if (empty($podsField_ProductLayers)) $podsField_ProductLayers = [];

    $podsField_ProductConfigData = pods_field('product', $productID, 'product_config_data', true);
    $podsField_ProductConfigData = str_replace('"{n    "', '{"', $podsField_ProductConfigData);
    $podsField_ProductConfigData = str_replace(',n    "', ',"', $podsField_ProductConfigData);
    $podsField_ProductConfigData = str_replace('"n}"', '"}', $podsField_ProductConfigData);

    if (empty($podsField_ProductConfigData)) $podsField_ProductConfigData = json_encode([]);

    $podsField_ProductDesignType = pods_field('product', $productID, 'product_design_type', true);
    if (empty($podsField_ProductDesignType)) $podsField_ProductDesignType = json_encode([]);

    $podsField_ProductManufacturer = pods_field('product', $productID, 'product_manufacturer', true);
    if (empty($podsField_ProductManufacturer)) $podsField_ProductManufacturer = json_encode([]);

    $layerList = [];
    foreach ($podsField_ProductLayers as $layer) {
        $layerList[$layer['ID']] = [
            'src' => get_the_post_thumbnail_url($layer['ID'], 'full'),
            'name' => $layer['post_title']
        ];
    }

    $productPriceObject = json_encode([]);
    switch ($podsField_ProductDesignType) {
        case 'color-only':
            $productPriceObject = pods_field('product', $productID, 'product_price_object', true);
            if (!empty($productPriceObject)) $productPriceObject = json_decode($productPriceObject, true);
            break;
        case 'fixed':
        case 'customized':
            $productPriceObject = get_option('customized-price-object');
            break;
    }

    $productDetail['product']['layers'] = $layerList;
    $productDetail['product']['configData'] = json_decode($podsField_ProductConfigData, true);
    $productDetail['product']['priceObject'] = $productPriceObject;
    $productDetail['product']['designType'] = $podsField_ProductDesignType;
    $productDetail['product']['manufacturerId'] = (is_array($podsField_ProductManufacturer) && isset($podsField_ProductManufacturer["ID"])) ? $podsField_ProductManufacturer["ID"] : '';
    return $productDetail['product'];
}

function getProducts($shapeSlug, $categorySlugs = [], $stock = 0, $page = 1)
{
    $taxQuery = [
        'relation' => 'AND',
        [
            'taxonomy' => 'pa_shape',
            'field' => 'slug',
            'terms' => [$shapeSlug],
            'include_children' => false,
        ],
    ];

    if (!empty($categorySlugs)) {
        $taxQuery[] = [
            'relation' => 'OR',
            [
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $categorySlugs,
                'include_children' => false,
            ]
        ];
    }

    $args = [
        'posts_per_page' => 3000,
        'paged' => $page,
        'post_type' => 'product',
        'post_status' => 'publish',
        'tax_query' => $taxQuery,
    ];

    $products = (array) get_posts($args);

    wp_reset_query();
    $wp_query = new WP_Query($args);
    $end = ($page > 0 && $wp_query->max_num_pages < $page) ? 1 : 0;

    foreach ($products as $index => $product) {
        if (!checkPermissionIfProductDefault((int) $product->ID)) {
            unset($products[$index]);
            continue;
        }
        $designType = pods_field('product', $product->ID, 'product_design_type', true);
        if ($stock == 1 && $designType != 'fixed') {
            unset($products[$index]);
            continue;
        }

        // Load product list with shape and referral size ONLY
        $sizes = pods_field('product', $product->ID, 'pa_' . $shapeSlug . '-size', true);
        if (empty($sizes)) {
            unset($products[$index]);
            continue;
        }
        $products[$index]->sizes = $sizes;
        $products[$index]->thumbImg = get_the_post_thumbnail_url($product->ID);
    }

    return ['products' => $products, 'page' => $page, 'end' => $end];
}

function getProductInfoByFilter($productId, $shapeSlug, $sizeSlug = null)
{
    $productId = trim($productId);
    if (!is_numeric($productId) || empty($productId)) {
        return [];
    }

    $shapeSlug = trim($shapeSlug);
    if (!is_string($shapeSlug) || empty($shapeSlug)) {
        return [];
    }

    $taxQuery = [
        'relation' => 'AND',
        [
            'taxonomy' => 'pa_shape',
            'field' => 'slug',
            'terms' => [$shapeSlug],
            'include_children' => false,
        ],
    ];

    if (!empty($sizeSlug)) {
        $taxQuery[] = [
            [
                'taxonomy' => "pa_$shapeSlug-size",
                'field' => 'slug',
                'terms' => [$sizeSlug],
                'include_children' => false,
            ],
        ];
    }

    $args = [
        'include' => [$productId],
        'post_type' => 'product',
        'post_status' => 'publish',
    ];

    if ($productId != get_option('blank-tile-configuration')) {
        $args['tax_query'] = $taxQuery;
    }

    $existedProduct = (array) get_posts($args);
    if (empty($existedProduct)) {
        return [];
    }
    $productDetail = getProductDetailById($productId);
    if (empty($productDetail)) return [];
    $selectedThickness = wc_get_product_terms($productId, 'pa_thickness', [
        'fields' => 'all'
    ]);
    $productDetail['selectedThickness'] = !empty($selectedThickness) ? $selectedThickness[0]->slug : '';
    $shape = get_term_by('slug', $shapeSlug, 'pa_shape');
    $productDetail['shape'] = [
        'slug' => $shape->slug,
        'name' => $shape->name
    ];
    //    if (!$g_isManager) {
    //        $tileSize = get_term_by('term_id', $g_sizeID, 'pa_' . $g_shapeSlug . '-size');
    //        if ($tileSize) {
    //            $_sizeSlug = $tileSize->slug;
    //            $_shapeSlug = $g_shapeSlug;
    //        }
    //    } else
    $selectedSize = wc_get_product_terms($productId, "pa_$shapeSlug-size", [
        'fields' => 'all'
    ]);

    $productDetail['sizes'] = [];
    foreach ($selectedSize as $size) {
        $productDetail['sizes'][] = [
            'term_id' => $size->term_id,
            'name' => $size->name . '',
            'slug' => $size->slug
        ];
    }

    $sizeSlug = empty($sizeSlug) ? $productDetail['sizes'][0]['slug'] : $sizeSlug;
    $productDetail['selectedSizeSlug'] = $sizeSlug;

    global $wpdb;
    $manuRelatedDatas = $wpdb->get_results($wpdb->prepare(
        "SELECT * FROM " . ($wpdb->prefix . 'tool_manufacturer_data') . " WHERE manu_id=%d AND shape_slug = %s ",
        $productDetail['manufacturerId'],
        $shapeSlug
    ));

    $sizeWithRatioVisualizer = [];
    foreach ($manuRelatedDatas as $idx => $_manuRelatedData) {
        foreach ($productDetail['sizes'] as $size) {
            if ($size['slug'] == $_manuRelatedData->size_slug) {
                $sizeWithRatioVisualizer[$size['slug']] = $_manuRelatedData->option_1;

                if ($sizeSlug == $_manuRelatedData->size_slug) {
                    $manuRelatedData = $_manuRelatedData;
                }
                break;
            }
        }
    }

    $productDetail['sizeWithRatioVisualizer'] = $sizeWithRatioVisualizer;

    if (!empty($manuRelatedData)) {
        $productDetail['manufacturerRelatedData'] = (array) $manuRelatedData;
        $thicknessList = $productDetail['manufacturerRelatedData']['thickness'];
        $defaultThicknessList = json_decode($thicknessList, true);
        if (!empty($defaultThicknessList)) {
            $productDetail['selectedThickness'] = !empty($productDetail['selectedThickness']) ? $productDetail['selectedThickness'] : $defaultThicknessList[0];
            asort($defaultThicknessList);
            $thicknessTxt = '[';
            $thicknessTxt .= implode(',', $defaultThicknessList);
            $thicknessTxt .= ']';
            $thicknessList = $thicknessTxt;
        }
        $productDetail['manufacturerRelatedData']['thickness'] = $thicknessList;

        if ($productDetail['designType'] == 'customized') {
            $base_price = $productDetail['manufacturerRelatedData']['base_price'];
            $priceAreaPair = [];
            foreach ($productDetail['priceObject']['priceAreaPair'] as $area => $price) {
                $priceAreaPair[$area] = $base_price - ($base_price * $price / 100);
            }
            $productDetail['priceObject']['priceAreaPair'] = $priceAreaPair;
        }
    }

    // $packaging = wp_get_object_terms($productId,  'pa_packaging', [
    //     'fields' => 'all'
    // ]);
    // if ($packaging instanceof WP_Error) {
    //     $packaging = '';
    // }
    // $productDetail['packaging'] = $packaging;

    $minOrder = array_keys($productDetail['priceObject']['priceAreaPair']);
    $productDetail['minimumOrder'] = reset($minOrder);

    $packaging = wc_get_product_terms($productId, 'pa_packaging', [
        'fields' => 'all'
    ]);
    // if ($packaging instanceof WP_Error) {
    //     $productDetail['packaging'] = $packaging = '';
    // } else {
    //     $productDetail['packaging'] = $packaging[0]->name;
    // }
    if ($packaging instanceof WP_Error) {
        $packaging = '';
    }
    $productDetail['packaging'] = $packaging[0]->name;

    $delivery = wc_get_product_terms($productId, 'pa_delivery-time', [
        'fields' => 'all'
    ]);
    // if ($delivery instanceof WP_Error) {
    //     $productDetail['delivery'] = $delivery = '';
    // } else {
    //     $productDetail['delivery'] = $delivery[0]->name;
    // }
    if ($delivery instanceof WP_Error) {
        $delivery = '';
    }
    $productDetail['delivery'] = $delivery[0]->name;

    $subtext_1 = wc_get_product_terms($productId, 'pa_subtext-1', [
        'fields' => 'all'
    ]);
    // if ($subtext_1 instanceof WP_Error) {
    //     $productDetail['subtext_1'] = $subtext_1 = '';
    // } else {
    //     $productDetail['subtext_1'] = $subtext_1[0]->name;
    // }
    if ($subtext_1 instanceof WP_Error) {
        $subtext_1 = '';
    }
    $productDetail['subtext_1'] = $subtext_1[0]->name;

    $subtext_2 = wc_get_product_terms($productId, 'pa_subtext-2', [
        'fields' => 'all'
    ]);
    // if ($subtext_2 instanceof WP_Error) {
    //     $productDetail['subtext_2'] = $subtext_2 = '';
    // } else {
    //     $productDetail['subtext_2'] = $subtext_2[0]->name;
    // }
    if ($subtext_2 instanceof WP_Error) {
        $subtext_2 = '';
    }
    $productDetail['subtext_2'] = $subtext_2[0]->name;

    $subtext_3 = wc_get_product_terms($productId, 'pa_subtext-3', [
        'fields' => 'all'
    ]);
    // if ($subtext_3 instanceof WP_Error) {
    //     $productDetail['subtext_3'] = $subtext_3 = '';
    // } else {
    //     $productDetail['subtext_3'] = $subtext_3[0]->name;
    // }
    if ($subtext_3 instanceof WP_Error) {
        $subtext_3 = '';
    }
    $productDetail['subtext_3'] = $subtext_3[0]->name;

    $neededFields = array_flip([
        // Default fields
        'title',  'id', 'description', 'short_description', 'categories', 'featured_src', 'images', 'in_stock', 'stock_quantity', 'managing_stock',
        // Extra fields
        'layers', 'configData', 'priceObject', 'designType', 'manufacturerId',
        'selectedThickness', 'manufacturerRelatedData', 'selectedSizeSlug',
        'sizes', 'shape',
        'packaging', 'minimumOrder', 'delivery', 'subtext_1', 'subtext_2', 'subtext_3',
        'sizeWithRatioVisualizer'
    ]);
    foreach ($productDetail as $field => $value) {
        if (!isset($neededFields[$field])) {
            unset($productDetail[$field]);
        }
    }
    return $productDetail;
}

function uploadFeaturedImageForProduct($productID = null, $imageName, $imageData, $isFeatured = false)
{
    $upload_dir = wp_upload_dir();

    // @new
    $upload_path = str_replace('/', DIRECTORY_SEPARATOR, $upload_dir['path']) . DIRECTORY_SEPARATOR;

    $img = $imageData;
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);

    $decoded = base64_decode($img);
    $hashed_filename  = md5($imageName . microtime()) . '_' . $imageName;
    // @new
    file_put_contents($upload_path . $hashed_filename, $decoded);
    //HANDLE UPLOADED FILE
    if (!function_exists('wp_handle_sideload')) {
        require_once(ABSPATH . 'wp-admin/includes/file.php');
    }

    // Without that I'm getting a debug error!?
    if (!function_exists('wp_get_current_user')) {
        require_once(ABSPATH . 'wp-includes/pluggable.php');
    }

    // @new
    $file             = array();
    $file['error']    = '';
    $file['tmp_name'] = $upload_path . $hashed_filename;
    $file['name']     = $hashed_filename . '.png';
    $file['type']     = 'image/png';
    $file['size']     = filesize($upload_path . $hashed_filename);

    // upload file to server
    // @new use $file instead of $image_upload
    $file_return      = wp_handle_sideload($file, array('test_form' => false));
    $filename = $file_return['file'];

    $attachment = array(
        'post_mime_type' => $file_return['type'],
        'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
        'post_content' => '',
        'post_status' => 'inherit',
        'guid' => $upload_dir['url'] . '/' . basename($filename)
    );
    $attach_id = wp_insert_attachment($attachment, $filename, 289);
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
    wp_update_attachment_metadata($attach_id, $attach_data);
    $image = wp_get_attachment_image_src($attach_id, 'full');
    if ($isFeatured && !is_null($productID) && $image[0]) {
        // determine if in the $media image we created, the string of the URL exists
        // if so, we found our image. set it as thumbnail
        set_post_thumbnail($productID, $attach_id);
        return true;
    }
    return $image;
}

function _upsertProduct(
    $upsertType,
    $defaultProductID,
    $data,
    $newProductName,
    $categories = null,
    $shapeAndSize = null,
    $featuredImage = null,
    $priceObject = null,
    $minOrder = null,
    $thicknesses = null,
    $delivery = null,
    $packaging = null,
    $customDesign = null,
    $weight = null,
    $terazzo = 0,
    $subtext1 = null,
    $subtext2 = null,
    $subtext3 = null,
    $productDesignType,
    $productManufacturer
) {
    if (empty($upsertType) || !in_array($upsertType, ['create', 'update'])) return [];
    // GET TEMP PRODUCT
    $woocommerce = WooAPI::getClient();
    if ($woocommerce instanceof Exception) {
        return null;
    }
    $productDefault = $woocommerce->get('products/' . $defaultProductID);
    $newProductName = stripslashes_deep($newProductName);

    $_productLayers = [];
    foreach ($data as $layerName => $layerData) {
        if (!empty($layerData['id']) && $layerName !== 'background-layer' && $layerData['duplicateID'] == 0) {
            $_productLayers[] = $layerData['id'];
        }
    }

    $_shapes = [];
    foreach ($shapeAndSize as $shape => $shapeData) {
        $_shapes['slug'][] = $shape;
        $_shapes['sizeSlug'][] = $shape . '-size';
    }

    $defaultAttributes = [];
    foreach ($productDefault['product']['attributes'] as $index => $at) {
        $defaultAttributes[$at['slug']] = $at['options'];
    }

    $langList = pll_languages_list();
    $currentLang = pll_current_language();
    $langCounter = count($langList);
    $catList = [];
    $relatedTranslation = [];
    $relatedTranslationUpsertType = [];
    $categories = !empty($categories) ? $categories : $productDefault['product']['categories'];
    for ($langIdx = 0; $langIdx < $langCounter; $langIdx++) {
        if ($langList[$langIdx] != $currentLang || $currentLang != pll_get_post_language($defaultProductID)) {
            $tmpTranslatedProduct = pll_get_post($defaultProductID, $langList[$langIdx]);
            if (empty($tmpTranslatedProduct) || get_post_status($tmpTranslatedProduct) === 'trash') {
                $relatedTranslationUpsertType[$langList[$langIdx]] = [
                    'upsertType' => 'create'
                ];
            } else {
                $relatedTranslationUpsertType[$langList[$langIdx]] = [
                    'productID' => $tmpTranslatedProduct,
                    'upsertType' => 'update'
                ];
            }
        } else {
            $relatedTranslationUpsertType[$langList[$langIdx]] = [
                'productID' => $defaultProductID,
                'upsertType' => 'update'
            ];
        }
        foreach ($categories as $idx => $catId) {
            $tmpCat = get_term_by('id', $catId, 'product_cat');
            if ($langList[$langIdx] != 'en') {
                $tmpTerm = get_terms([
                    'taxonomy' => 'product_cat',
                    'lang' => $langList[$langIdx],
                    'slug' => $tmpCat->slug . '-' . $langList[$langIdx],
                    'hide_empty' => false
                ]);
            } else {
                $tmpTerm = get_terms([
                    'taxonomy' => 'product_cat',
                    'lang' => 'en',
                    'slug' => $tmpCat->slug,
                    'hide_empty' => false
                ]);
            }
            $catList[$langList[$langIdx]][] = $tmpTerm[0]->term_id;
        }
    }

    try {
        for ($langIdx = 0; $langIdx < $langCounter; $langIdx++) {
            if ($upsertType === 'create' || $relatedTranslationUpsertType[$langList[$langIdx]]['upsertType'] === 'create') {
                $tmpProduct = $woocommerce->post('products', $productDefault);
            } else {
                $tmpProduct = $productDefault;
                if ($langList[$langIdx] != $currentLang || $currentLang != pll_get_post_language($defaultProductID)) {
                    $tmpProduct = $woocommerce->get('products/' . $relatedTranslationUpsertType[$langList[$langIdx]]['productID']);
                }
            }
            // d($tmpProduct);
            // Set categories for new product
            wp_set_object_terms($tmpProduct['product']['id'], $catList[$langList[$langIdx]], 'product_cat');
            // Set shape and size for new product
            if (!empty($shapeAndSize)) {
                // Get default attributes and build new attributes for new product
                $newProductAttributes = [];
                // $productAttributes = get_post_meta($productDefault['product']['id'], '_product_attributes');
                // foreach($productAttributes[0] as $i => $attData) {
                //     $_slug = str_replace('pa_', '', $attData['name']);
                //     if(preg_match('/-size/', $attData['name']) || preg_match('/shape/', $attData['name'])) continue;

                //     wp_set_object_terms($productDefault['product']['id'], $defaultAttributes[$_slug], $attData['name']);
                //     $newProductAttributes[] = [
                //         'name'=> $attData['name'],
                //         'value'=> ($attData['name'] === 'pa_minimum-order' ? [$minOrder] : $defaultAttributes[$_slug]),
                //         'is_visible' => '1',
                //         'is_taxonomy' => '1'
                //     ];
                // }

                // Build size attributes for new product
                foreach ($shapeAndSize as $shape => $shapeData) {
                    // Set shape for new product
                    wp_set_object_terms($tmpProduct['product']['id'], [$shape], 'pa_shape');
                    $newProductAttributes[] = [
                        'name' => 'pa_shape',
                        'value' => $shape,
                        'is_visible' => '1',
                        'is_taxonomy' => '1'
                    ];

                    wp_set_object_terms($tmpProduct['product']['id'], $shapeData['size'], 'pa_' . $shape . '-size');
                    $newProductAttributes[] = [
                        'name' => 'pa_' . $shape . '-size',
                        'value' => $shapeData['size'],
                        'is_visible' => '1',
                        'is_taxonomy' => '1'
                    ];
                }
            }

            // Set thickness for new product
            if (!empty($thicknesses)) {
                wp_set_object_terms($tmpProduct['product']['id'], $thicknesses, 'pa_thickness');
                $newProductAttributes[] = [
                    'name' => 'pa_thickness',
                    'value' => $thicknesses,
                    'is_visible' => '1',
                    'is_taxonomy' => '1'
                ];
            }

            // Set delivery for new product
            if (!empty($delivery)) {
                wp_set_object_terms($tmpProduct['product']['id'], stripslashes_deep($delivery), 'pa_delivery-time');
                $newProductAttributes[] = [
                    'name' => 'pa_delivery-time',
                    'value' => stripslashes_deep($delivery),
                    'is_visible' => '1',
                    'is_taxonomy' => '1'
                ];
            }

            if (!empty($packaging)) {
                wp_set_object_terms($tmpProduct['product']['id'], stripslashes_deep($packaging), 'pa_packaging');
                $newProductAttributes[] = [
                    'name' => 'pa_packaging',
                    'value' => stripslashes_deep($packaging),
                    'is_visible' => '1',
                    'is_taxonomy' => '1'
                ];
            }

            if (!empty($customDesign)) {
                wp_set_object_terms($tmpProduct['product']['id'], stripslashes_deep($customDesign), 'pa_custom-design');
                $newProductAttributes[] = [
                    'name' => 'pa_custom-design',
                    'value' => stripslashes_deep($customDesign),
                    'is_visible' => '1',
                    'is_taxonomy' => '1'
                ];
            }

            // Set weight of tile for new product
            if (!empty($weight)) {
                wp_set_object_terms($tmpProduct['product']['id'], $weight, 'pa_weight-per-piece');
                $newProductAttributes[] = [
                    'name' => 'pa_weight-per-piece',
                    'value' => $weight,
                    'is_visible' => '1',
                    'is_taxonomy' => '1'
                ];
            }

            // Set pieces of m2 for new product
            if (!empty($pieceOfM2)) {
                wp_set_object_terms($tmpProduct['product']['id'], $pieceOfM2, 'pa_piece-of-m2');
                $newProductAttributes[] = [
                    'name' => 'pa_piece-of-m2',
                    'value' => $pieceOfM2,
                    'is_visible' => '1',
                    'is_taxonomy' => '1'
                ];
            }

            if (!empty($minOrder)) {
                $minOrderTerm = [
                    'name' => $minOrder
                ];
                wp_set_object_terms($tmpProduct['product']['id'], $minOrderTerm, 'pa_minimum-order');
                $newProductAttributes[] = [
                    'name' => 'pa_minimum-order',
                    'value' => $minOrderTerm,
                    'is_visible' => '1',
                    'is_taxonomy' => '1'
                ];
            }

            if (!empty($subtext1)) {
                wp_set_object_terms($tmpProduct['product']['id'], $subtext1, 'pa_subtext-1');
                $newProductAttributes[] = [
                    'name' => 'pa_subtext-1',
                    'value' => $subtext1,
                    'is_visible' => '1',
                    'is_taxonomy' => '1'
                ];
            }

            if (!empty($subtext2)) {
                wp_set_object_terms($tmpProduct['product']['id'], $subtext2, 'pa_subtext-2');
                $newProductAttributes[] = [
                    'name' => 'pa_subtext-2',
                    'value' => $subtext2,
                    'is_visible' => '1',
                    'is_taxonomy' => '1'
                ];
            }

            if (!empty($subtext3)) {
                wp_set_object_terms($tmpProduct['product']['id'], $subtext3, 'pa_subtext-3');
                $newProductAttributes[] = [
                    'name' => 'pa_subtext-3',
                    'value' => $subtext3,
                    'is_visible' => '1',
                    'is_taxonomy' => '1'
                ];
            }

            //        $podsField_ProductConfigData = pods_field ( 'product', $tmpProduct['product']['id'], 'product_config_data', true );
            //        if($upsertType != 'create'
            //            && (stripslashes_deep(json_encode($data)) == stripslashes_deep($podsField_ProductConfigData))
            //            && !empty($tmpProduct['product']['featured_src'])) {
            //            $featuredImage = null;
            //        }

            // Update new attributes for new product
            update_post_meta($tmpProduct['product']['id'], '_product_attributes', $newProductAttributes);

            // Update featured image into new product
            if (!empty($featuredImage)) {
                uploadFeaturedImageForProduct($tmpProduct['product']['id'], $newProductName, $featuredImage, true);
            }

            $priceObject = !is_null($priceObject) ? $priceObject : [];
            if (!empty($priceObject)) {
                ksort($priceObject['priceAreaPair']);
            }

            // Set layers and config data for new product
            $myPods = pods('product');
            $myPods->add_to('related_item_id', $_productLayers);
            $myPods->add_to('related_item_id', $productManufacturer);
            $myPods->save([
                'product_layer' => $_productLayers,
                'product_config_data' => json_encode($data),
                'product_price_object' => json_encode($priceObject),
                'product_design_type' => $productDesignType,
                'product_manufacturer' => $productManufacturer,
                'product_terazzo' => $terazzo ?? 0
            ], null, $tmpProduct['product']['id']);

            // Update the post title into the database
            wp_update_post([
                'ID'           => $tmpProduct['product']['id'],
                'post_title'   => esc_sql($newProductName)
            ]);
            if ($upsertType === 'create' || $relatedTranslationUpsertType[$langList[$langIdx]]['upsertType'] === 'create') {
                pll_set_post_language($tmpProduct['product']['id'], $langList[$langIdx]);
            }
            $relatedTranslation[$langList[$langIdx]] = $tmpProduct['product']['id'];
        }
        pll_save_post_translations($relatedTranslation);
        return true;
    } catch (Exception $e) {
        return false;
    }
}

function getTileToolShareLink($shareID, $selectCols = [])
{
    $selectCols = (!empty($selectCols) && is_array($selectCols)) ? implode(',', $selectCols) : '*';

    global $wpdb;
    $result = $wpdb->get_row($wpdb->prepare(
        "SELECT " . $selectCols . " FROM " . ($wpdb->prefix . 'tool_share_links') . " WHERE id=%d",
        $shareID
    ));

    return $result;
}
