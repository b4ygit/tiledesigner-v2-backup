<?php

function registerDomToImageJS() {
    wp_register_script( 'domToImageJS', plugins_url( 'assets/dom-to-image.min.js', __FILE__ ), ['jquery-core']);
    wp_enqueue_script( 'domToImageJS' );
}

function registerCustomJs() {
    wp_register_script( 'customJS', plugins_url( 'assets/customJS.js', __FILE__ ), ['jquery-core']);
    wp_enqueue_script( 'customJS' );
    $params = [];
    $g_action = !empty($_GET['action']) ? $_GET['action'] : null;
    $g_isManager = !empty($_GET['isManager']) ? $_GET['isManager'] : null;
    $g_productID = !empty($_GET['productID']) ? $_GET['productID'] : null;
    if(!empty($g_action) && $g_action == 'shareLink') {
        $g_linkID = !empty($_GET['id']) ? $_GET['id'] : null;
        $linkResult = getTileToolShareLink($g_linkID, ['shapeSlug', 'sizeID', 'catID', 'productID']);
        if(!empty($linkResult)) {
            $params['action'] = 'productDetail';
            $params['shapeSlug'] = $linkResult->shapeSlug;
            $params['sizeID'] = $linkResult->sizeID;
            $cat = getCatById($linkResult->catID);
            $params['catSlug'] = $cat->slug;
            $params['productID'] = $linkResult->productID;
        }
    } elseif(!empty($g_action) && $g_action == 'editCartItem') {
        $g_orderItemId = !empty($_GET['itemId']) ? $_GET['itemId'] : null;
        $g_orderId = !empty($_GET['orderId']) ? $_GET['orderId'] : null;
        $cartData = getTilesShopCheckout($g_orderId);

        if(!empty($cartData) && !empty($cartData[$g_orderItemId])) {
            $orderItemData = $cartData[$g_orderItemId];
            $oid_ProductDetail = $orderItemData['productDetail'];

            $params['action'] = 'productDetail';
            $params['shapeSlug'] = $oid_ProductDetail['shapeSlug'];
            $params['sizeID'] = $oid_ProductDetail['sizeID'];
            $params['catID'] = $oid_ProductDetail['catID'];
            $params['productID'] = $oid_ProductDetail['productID'];
        }
    } else {
        $params = $_GET;
        $shapeName = wc_get_product_terms($g_productID, 'pa_shape');
        if($g_isManager && !empty($shapeName)) {
            $shapeObject = get_term_by('name', reset($shapeName), 'pa_shape');
            $params['shapeSlug'] = $shapeObject->slug;
        }
    }

    wp_localize_script( 'customJS', 'customJSParams', array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'url' => get_page_link(),
        'params' => $params
    ));
}

function tiles_plugin_assets() {
    wp_register_script(
        'tiles-bundle', 
        plugins_url('assets/dist/js/bundle.min.js', __FILE__), 
        [],
        '2.0',
        true
    );
    wp_enqueue_script('tiles-bundle');

    wp_register_script(
        'tiles-app', 
        plugins_url('assets/dist/js/app.min.js', __FILE__), 
        [],
        '2.0',
        true
    );
    wp_enqueue_script('tiles-app');
    wp_localize_script('tiles-app', 'wpParams', [
        'ajax_url' => admin_url('admin-ajax.php'),
    ]);
    
    wp_register_style( 'normalize-style', plugins_url( 'assets/new-assets/css/normalize.css', __FILE__ ));
    wp_enqueue_style( 'normalize-style' );

    wp_register_style( 'main-style', plugins_url( 'assets/new-assets/css/main.css', __FILE__ ));
    wp_enqueue_style( 'main-style' );

    wp_register_style( 'bootstrap-style', plugins_url( 'assets/plugins/bootstrap/css/bootstrap.min.css', __FILE__ ));
    wp_enqueue_style( 'bootstrap-style' );

    wp_register_style( 'gothamfont-style', plugins_url( 'assets/new-assets/fonts/gotham.css', __FILE__ ));
    wp_enqueue_style( 'gothamfont-style' );

    wp_register_style( 'googlefont-style', 'https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&amp;subset=vietnamese');
    wp_enqueue_style( 'googlefont-style' );

    wp_register_style( 'fontawesome-style', plugins_url( 'assets/plugins/font-awesome/css/fontawesome-all.min.css', __FILE__ ));
    wp_enqueue_style( 'fontawesome-style' );

    wp_register_style( 'swiper-style', plugins_url( 'assets/plugins/swiper/css/swiper.min.css', __FILE__ ));
    wp_enqueue_style( 'swiper-style' );

    wp_register_style( 'magnific-popup-style', plugins_url( 'assets/plugins/magnific-popup/magnific-popup.css', __FILE__ ));
    wp_enqueue_style( 'magnific-popup-style' );

    wp_register_style( 'nouisslider-style', plugins_url( 'assets/plugins/sliderrange/nouislider.min.css', __FILE__ ));
    wp_enqueue_style( 'nouisslider-style' );

    wp_register_style( 'themify-icons-style', plugins_url( 'assets/plugins/themify-icons/themify-icons.css', __FILE__ ));
    wp_enqueue_style( 'themify-icons-style' );

    wp_register_style( 'nice-select-style', plugins_url( 'assets/plugins/nice-select/nice-select.css', __FILE__ ));
    wp_enqueue_style( 'nice-select-style' );

    wp_register_style( 'bootstrap-slider-style', plugins_url( 'assets/js/bootstrap-slider/css/bootstrap-slider.min.css', __FILE__ ));
    wp_enqueue_style( 'bootstrap-slider-style' );

    wp_register_style( 'videojs-style', plugins_url( 'assets/plugins/video/video-js.min.css', __FILE__ ));
    wp_enqueue_style( 'videojs-style' );

    wp_register_style( 'tiles-style', plugins_url( 'assets/new-assets/css/styles.css', __FILE__ ));
    wp_enqueue_style( 'tiles-style' );

    wp_register_style( 'design-tool', plugins_url( 'assets/css/designToolCSS_v2.css', __FILE__ ));
    wp_enqueue_style( 'design-tool' );
}

function tiles_dequeue_styles_scripts() {
    wp_dequeue_script( 'wc-add-to-cart' ); // woocommerce
    wp_dequeue_script( 'jquery-blockui' ); // woocommerce
    wp_dequeue_script( 'js-cookie' ); // woocommerce
    wp_dequeue_script( 'woocommerce' ); // woocommerce
    wp_dequeue_script( 'jquery-cookie' ); // woocommerce
    wp_dequeue_script( 'wc-cart-fragments' ); // woo-poly-integration


    wp_dequeue_style( 'wp-block-library' ); // woocommerce
    wp_dequeue_style( 'wc-block-vendors-style' ); // woocommerce
    wp_dequeue_style( 'wc-block-style' ); // woocommerce
    wp_dequeue_style( 'woocommerce-inline' );
}

function registerThemeStyleCss() {
    wp_register_style( 'tiles-theme-style', plugins_url( 'theme/style/style.css', __FILE__ ));
    wp_enqueue_style( 'tiles-theme-style' );
}

function wpse_add_custom_meta_box_2() {
    add_meta_box(
        'custom_tile_tool_product',      // $id
        'Customize Pattern',             // $title
        // 'show_backend_tool',             // $callback
        'product',                       // $page
        'normal',                        // $context
        'high'                           // $priority
    );
}

function myStartSession() {
    if(!session_id()) session_start();
}
//
function my_edit_manufacturer_columns( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Title', 'tile-tool'),
        'editLink' => __( 'Edit Link' , 'tile-tool')
    );

    return $columns;
}
//
function my_manage_manufacturer_columns( $column, $post_id ) {
    switch( $column ) {
        /* If displaying the 'editLink' column. */
        case 'editLink' :
            $link = get_permalink(get_page_by_path('tile-tool')) . '?action=manufacturerEdit&manuID=' . $post_id;
            echo __('<a href="' . $link  . '" target="_blank">' . __('EDIT DATA', 'tile-tool') . '</a>');
            break;
        /* Just break out of the switch statement for everything else. */
        default :
            break;
    }
}
//
function my_edit_layer_columns( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Title' , 'tile-tool'),
        'layerImage' => __( 'Image' , 'tile-tool')
    );

    return $columns;
}
//
function my_manage_layer_columns( $column, $post_id ) {
    switch( $column ) {
        /* If displaying the 'editLink' column. */
        case 'layerImage' :
            $imageSrc = get_the_post_thumbnail_url($post_id, 'thumbnail');
            echo __('<img src="' . $imageSrc . '" width="40" height="40" />', 'tile-tool');
            break;
        /* Just break out of the switch statement for everything else. */
        default :
            break;
    }
}
//
function filter_woocommerce_order_item_name( $item_name, $item ) {
    $product_permalink = pods_field ( 'shop_order', $item->get_order()->get_id(), 'share_link', true );
    $item_name = $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item->get_name() ) : $item->get_name();
    return $item_name;
};

//
add_action('add_meta_boxes', 'wpse_add_custom_meta_box_2');
//
add_action( 'wp_ajax_nopriv_getSizeByShape', 'getSizeByShape' );
add_action( 'wp_ajax_getSizeByShape', 'getSizeByShape' );
//
add_action( 'wp_ajax_nopriv_getProductsByFilter', 'getProductsByFilter' );
add_action( 'wp_ajax_getProductsByFilter', 'getProductsByFilter' );
//
add_action( 'wp_ajax_nopriv_makeLink', 'makeLink' );
add_action( 'wp_ajax_makeLink', 'makeLink' );
//
add_action( 'wp_ajax_nopriv_addToCart', 'addToCart' );
add_action( 'wp_ajax_addToCart', 'addToCart' );
//
add_action( 'wp_ajax_nopriv_upsertProduct', 'upsertProduct' );
add_action( 'wp_ajax_upsertProduct', 'upsertProduct' );
//
add_action( 'wp_ajax_nopriv_loadMoreLayer', 'loadMoreLayer' );
add_action( 'wp_ajax_loadMoreLayer', 'loadMoreLayer' );
//
add_action( 'wp_ajax_nopriv_loadColorSelectionBlock', 'loadColorSelectionBlock' );
add_action( 'wp_ajax_loadColorSelectionBlock', 'loadColorSelectionBlock' );
//
add_action( 'wp_ajax_nopriv_loadColorSelectionList', 'loadColorSelectionList' );
add_action( 'wp_ajax_loadColorSelectionList', 'loadColorSelectionList' );
//
add_action( 'wp_ajax_nopriv_calTotalBill', 'calTotalBill' );
add_action( 'wp_ajax_calTotalBill', 'calTotalBill' );
//
add_action( 'wp_ajax_nopriv_placeOrder', 'placeOrder' );
add_action( 'wp_ajax_placeOrder', 'placeOrder' );
//
add_action( 'wp_ajax_nopriv_getShippingZonesByCountry', 'getShippingZonesByCountry' );
add_action( 'wp_ajax_getShippingZonesByCountry', 'getShippingZonesByCountry' );
//
add_action( 'wp_ajax_nopriv_getShapeSizeByManufacturer', 'getShapeSizeByManufacturer' );
add_action( 'wp_ajax_getShapeSizeByManufacturer', 'getShapeSizeByManufacturer' );
//
add_action( 'wp_ajax_nopriv_getThicknessManagerList', 'getThicknessManagerList' );
add_action( 'wp_ajax_getThicknessManagerList', 'getThicknessManagerList' );
//
add_action( 'wp_ajax_nopriv_getManuShapeSizeRelatedData', 'getManuShapeSizeRelatedData' );
add_action( 'wp_ajax_getManuShapeSizeRelatedData', 'getManuShapeSizeRelatedData' );
//
add_action( 'wp_ajax_nopriv_getGalleries', 'getGalleries' );
add_action( 'wp_ajax_getGalleries', 'getGalleries' );
//
add_action( 'wp_ajax_nopriv_getHtmlFragment', 'getHtmlFragment' );
add_action( 'wp_ajax_getHtmlFragment', 'getHtmlFragment' );
//
add_action( 'wp_ajax_nopriv_addToWishlist', 'addToWishlist' );
add_action( 'wp_ajax_addToWishlist', 'addToWishlist' );
//
add_action( 'wp_ajax_upsertCustomizedPriceObject', 'upsertCustomizedPriceObject' );
add_action( 'wp_ajax_upsertRulerConfiguration', 'upsertRulerConfiguration' );
add_action( 'wp_ajax_upsertColorHoverConfiguration', 'upsertColorHoverConfiguration' );
//
add_action( 'wp_ajax_nopriv_getLayerMinMaxData', 'getLayerMinMaxData' );
add_action( 'wp_ajax_getLayerMinMaxData', 'getLayerMinMaxData' );
//
add_action( 'wp_ajax_nopriv_removeOrderItem', 'removeOrderItem' );
add_action( 'wp_ajax_removeOrderItem', 'removeOrderItem' );
//
add_action( 'wp_ajax_nopriv_editOrderItem', 'editOrderItem' );
add_action( 'wp_ajax_editOrderItem', 'editOrderItem' );
//
add_action( 'wp_ajax_nopriv_removeWishlistItem', 'removeWishlistItem' );
add_action( 'wp_ajax_removeWishlistItem', 'removeWishlistItem' );
//
add_action( 'wp_enqueue_scripts', 'tiles_dequeue_styles_scripts', 110);
add_filter( 'woocommerce_enqueue_styles', '__return_false' );
//
add_action('init', 'myStartSession', 1); 

add_action( 'wp_enqueue_scripts', 'tiles_plugin_assets' );
add_action( 'manage_manufacturer_posts_custom_column', 'my_manage_manufacturer_columns', 10, 2 );
add_action( 'manage_layer_posts_custom_column', 'my_manage_layer_columns', 10, 2 );
//
add_filter( 'manage_edit-manufacturer_columns', 'my_edit_manufacturer_columns' ) ;
//
add_filter( 'manage_edit-layer_columns', 'my_edit_layer_columns' ) ;
//
add_filter( 'woocommerce_order_item_name', 'filter_woocommerce_order_item_name', 10, 2 );
