<?php

function renderCategories()
{
    $html = '';

    $categories = get_terms([
        'taxonomy' => 'product_cat',
        'hide_empty' => false,
    ]);

    foreach ($categories as $category) {
        $html .= '<label>
                    <input type="checkbox" autocomplete="off" name="category" value="'.$category->slug.'" class="checkbox-pattern">
                    <div class="checkbox-pattern-content">
                        '.$category->name.' <i class="ti-close"></i>
                    </div>
                </label>';
    }

    return $html;
}

function renderProduct($shapeSlug)
{
    $html = '';
    $blankProductId = get_option('blank-tile-configuration');

    $productResp = getProducts($shapeSlug);
    $currentLang = pll_current_language();
    foreach ($productResp['products'] as $idx => $product) {
        if ($blankProductId == $product->ID && (!current_user_can('administrator') || !current_user_can("manage_tiles_shop"))) {
            unset($productResp['products'][$idx]);
            continue;
        }
        if ($blankProductId != $product->ID && pll_get_post_language($product->ID) != $currentLang) {
            continue;
        }
        $html .= '<div class="col-auto mb-3">
                    <label>
                        <input type="radio" autocomplete="off" name="product" value="'.$product->ID.'" class="choose-shape">
                        <div class="design-block text-center rounded">
                            <div class="img-design">
                                <img src="'.$product->thumbImg.'" alt="">
                            </div>
                            <h6>'.$product->post_title.'</h6>
                        </div>
                    </label>
                </div>';
    }

    return $html;
}
