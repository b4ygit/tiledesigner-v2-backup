<?php

require_once 'view_type_funcs.php';
require_once 'tool_funcs.php';

$shapes = renderShapes_v2();

$error_message = $_SESSION['type-error-message'] ?? null;
if (!empty($error_message)) {
  unset($_SESSION['type-error-message']);
}

$header = renderHeader('type');
$menu = renderCollapseMenu();

$html = <<<HTML
  <div id="root">
    
    $header
    
    <!-- $menu -->

    <main id="main" class="view-type-template">
      <section class="header-block position-relative reset-max-width" id="headerTypeTool">
        <div class="container pt-2">
          <div class="title-box">
            <h3 class="tool-title">DESIGN YOUR PERSONAL CEMENT TILE </h3>
            <p class="paragraph">It's time to start designing your own dream tiles! Our tool will offer you complete freedom in customizing your design and let you easily create your unique patterns. If you want to visualize how the tiles will look in real-life contexts, our visualizer is here to help with many rooms mockups and presets of any possible pattern over all shapes!</p>
          </div>
          <div class="checklist-group">
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="check-list first-list">
                  <ul class="hiw-list">
                    <li>Get your own unique design in minutes</li>
                    <li>Use 800 elements and over 100 colors</li>
                    <li>Select from 10 shapes and many sizes</li>
                  </ul>
                  <div class="btn-group">
                    <a target="_blank" href="https://www.tiles.design/custom-tile" class="link blue-style mt-2">FIND OUT HOW IT WORKS</a>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <div class="check-list">
                  <ul class="hiw-list">
                    <li>GET ISO 9001 certified top quality Tiles</li>
                    <li>FREE SHIPPING TO MOST COUNTRIES/ORDERS</li>
                    <li>MANY SAFE AND SECURE PAYMENT METHODS</li>
                  </ul>
                  <div class="btn-group">
                    <div class="row">
                      <div class="col-auto"><a target="_blank" href="https://www.tiles.design/samples" class="link blue-style mt-2">ORDER DEMO TILES</a></div>
                      <div class="col-auto"><a target="_blank" href="https://www.tiles.design/pricing" class="link mt-2">SEE PRICING AND DISCOUNTS</a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="select-shape-block">
        <div class="container-fluid shape-container">
           <div class="title-box">
            <h3 class="tool-title">SELECT A SHAPE TO START</h3>
            <p class="paragraph">We offer 10 different shape to make your project unique. Each shape offers many unique patterns which you can try in our visualizer.Also we offer many possible sizes per shape which you can select after you saved your design</p>
          </div>
            <div class="row justify-content-center">
             $shapes
            </div>
        </div>
      </section>
    </main>

    <footer class="footer-design">
      <div class="footer-design-wrap border-top" id="footer-design-wrap">
        <p class="text-welcome m-0">Welcome to Tiles.Design - Select a shape to start!</p>
        <a href="javascript:void(0);" class="link blue-style large btn-next-step disabled" id="btn-next-step">NEXT STEP</a>
      </div>
    </footer>
  </div>

HTML;

echo $html;
