<?php

function renderShapes_v2()
{
    $html = '';

    $_shapes = get_terms([
        'taxonomy' => 'pa_shape',
        'hide_empty' => false,
    ]);

    foreach ($_shapes as $_shape) {
        $_shapeSlug = $_shape->slug;
        // Get sizes for checking valid shape
        $_sizes = get_terms([
            'taxonomy' => "pa_$_shapeSlug-size",
            'hide_empty' => false,
        ]);
        if (empty($_sizes) || !empty($_sizes->errors)) {
            continue;
        }
        // Get extra data for term and taxonomy
        $shapeImageSvg = pods_field('pa_shape', $_shape->term_id, 'shape_image_svg', true);
        $shapeImageSvg = $shapeImageSvg['guid'];

        $shapeImageActiveSvg = pods_field('pa_shape', $_shape->term_id, 'shape_image_active_svg', false);
        $shapeImageActiveSvg = $shapeImageActiveSvg['guid'];

        $shapeImagePng = pods_field('pa_shape', $_shape->term_id, 'shape_image_png', false);
        $shapeImagePng = $shapeImagePng['guid'];

        $shapeImageHoverSvg = pods_field('pa_shape', $_shape->term_id, 'shape_image_hover_svg', false);
        $shapeImageHoverSvg = $shapeImageHoverSvg['guid'];

        $html .= '<div class="col-auto shape-col">
              <label>
                <input type="radio" name="choose-shape" autocomplete="off" class="choose-shape" value="'.$_shapeSlug.'">
                <div class="tiles-block shape-block show-first-time text-center border-top border-left border-right rounded-top">
                  <div class="img-shape">
                    <img src="'.$shapeImageSvg.'" alt="shapeImageSvg" class="img-default">
                    <img src="'.$shapeImageActiveSvg.'" alt="shapeImageActiveSvg" class="img-active">
                    <img src="'.$shapeImageHoverSvg.'" alt="shapeImageHoverSvg" class="img-hover">
                  </div>
                  <h6 class="name-shape">'.__($_shape->name, 'tile-tool').'</h6>
                </div>
                <div class="rounded-bottom">
                  <img src="'.$shapeImagePng.'" alt="shapeImagePng" class="shape-thumb img-thumb">
                </div>
              </label>
            </div>';
    }

    return $html;
}